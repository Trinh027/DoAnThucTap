let { getOrderByKey, saveBankingAPI, updateOrderStatus, setWithdrawal } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function findOrder(data, flat) {
    if (flat) {
        saveBankingAPI(data)
    }
    let key = data.note.split(" ")[1];
    getOrderByKey(key)
        .then(result => {
            if (result) {
                if (result.STATUS == 0 && data.value == result.TOTAL) {
                    parseFloat
                    updateOrderStatus(result.CREATETIME, 1, result.ID)
                    let { ID, KEY, CREATETIME, WALLET, AMOUNT, TYPE } = result;
                    return setWithdrawal({ ID, KEY, CREATETIME, WALLET, AMOUNT: parseFloat(AMOUNT), TYPE })
                        .catch(err => {
                            console.log(err)
                            addLogToList({ folder: "inputBankingAPI", text: `Error - setWithdrawal=${err.toString()}` });
                            setTimeout(() => {
                                findOrder(data, false)
                            }, 10000)
                        })
                } else {
                    return updateOrderStatus(result.CREATETIME, -1, result.ID)
                }
            }
        })
        .catch(err => {
            console.log(err)
            setTimeout(() => {
                findOrder(data, false)
            }, 10000)
        })
}

function inputBankingAPI(req, res) {
    findOrder(req.body, true)
    return res.send("Okay!")
}

module.exports = inputBankingAPI;