let { addOrder } = require("../../controller/Firebase.js");
let { getPrice } = require("../../controller/System.js");
let { getSocketByID } = require("../../controller/User.js");
let { checkBankAccount } = require("../../controller/Axios.js");
let { sendNewOrder } = require("../../controller/Order.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function sellorder(req, res) {
    console.log(req.body);
    let { ID, SOCKET, AMOUNT, ACCOUNT } = req.body;
    if (!ID) {
        addLogToList({ folder: "sellorder", text: `Error - ID=${ID}` });
        return res.send(JSON.stringify({ STATUS: 0, NOTE: "Wrong ID!" }));
    }
    let testAmount = parseFloat(AMOUNT);
    if (!testAmount && testAmount < 0) {
        addLogToList({ folder: "sellorder", text: `Error - AMOUNT=${AMOUNT} - ID=${ID}` });
        return res.send(JSON.stringify({ STATUS: 0, NOTE: "Wrong Amount Of BTC!" }));
    }
    let socket = getSocketByID(ID);
    if (!socket || socket.id != SOCKET) {
        addLogToList({ folder: "sellorder", text: `Error - SOCKETID=${SOCKET} - ID=${ID}` });
        return res.send(JSON.stringify({ STATUS: 0, NOTE: "Wrong SOCKET ID!" }));
    }
    return checkBankAccount(ACCOUNT)
        .then(result => {
            if (parseInt(result.STATUS) != 1) {
                let NOTE = parseInt(result.STATUS) == 2 ? result.FULLNAME : result.NOTE;
                addLogToList({ folder: "sellorder", text: `Error - BANKACCOUNT=${ACCOUNT} - ID=${ID}` });
                return res.send(JSON.stringify({ STATUS: 0, NOTE }));
            }
            let price = getPrice();
            let total = parseInt(req.body.AMOUNT * price.SELLPRICE);
            let obj = { ID, AMOUNT, TYPE: "SELL", ACCOUNT, TOTAL: total, STATUS: 0 };
            return addOrder(obj)
                .then(data => {
                    sendNewOrder(socket, obj)
                    addLogToList({ folder: "sellorder", text: `Success - addOrder=${obj.KEY}` });
                    return res.send(JSON.stringify({ STATUS: 1, DATA: obj }));
                })
                .catch(err => {
                    console.log(err.toString());
                    addLogToList({ folder: "sellorder", text: `Error - addOrder=${err.toString()} - ID=${ID}` });
                    return res.send(JSON.stringify({ STATUS: 0, NOTE: "Database Error!" }));
                })
        })
        .catch(err => {
            console.log(err.toString())
            addLogToList({ folder: "sellorder", text: `Error - checkBankAccount=${err.toString()} - ID=${ID}` });
            return res.send(JSON.stringify({ STATUS: 0, NOTE: "Fail When Check Bank Account!" }));
        })
}

module.exports = sellorder;