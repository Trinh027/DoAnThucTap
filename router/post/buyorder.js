let { addOrder } = require("../../controller/Firebase.js");
let { getPrice } = require("../../controller/System.js");
let { getSocketByID } = require("../../controller/User.js");
let { sendNewOrder } = require("../../controller/Order.js");
let { addLogToList } = require("../../controller/LogWriter.js")
const isValid = wallet => require("../../controller/bitcore.js").bitcore.Address.isValid(wallet, 'livenet');

function buyorder(req, res) {
    let { ID, SOCKET, AMOUNT, WALLET } = req.body;
    if (!ID) {
        addLogToList({ folder: "buyorder", text: `Error - ID=${ID}` });
        return res.send(JSON.stringify({ STATUS: 0, NOTE: "Wrong ID!" }));
    }
    if (!isValid(WALLET)) {
        addLogToList({ folder: "buyorder", text: `Error - WALLET=${WALLET} - ID=${ID}` });
        return res.send(JSON.stringify({ STATUS: 0, NOTE: "Wrong Wallet!" }));
    }
    let testAmount = parseFloat(AMOUNT);
    if (!testAmount && testAmount < 0) {
        addLogToList({ folder: "buyorder", text: `Error - AMOUNT=${AMOUNT} - ID=${ID}` });
        return res.send(JSON.stringify({ STATUS: 0, NOTE: "Wrong Amount Of BTC!" }));
    }
    let socket = getSocketByID(ID);
    if (!socket || socket.id != SOCKET) {
        addLogToList({ folder: "buyorder", text: `Error - SOCKETID=${SOCKET} - ID=${ID}` });
        return res.send(JSON.stringify({ STATUS: 0, NOTE: "Wrong SOCKET ID!" }));
    }
    let price = getPrice();
    let total = parseInt(req.body.AMOUNT * price.BUYPRICE);
    let obj = { ID, AMOUNT: parseFloat(AMOUNT), TYPE: "BUY", WALLET, TOTAL: total, STATUS: 0 };
    return addOrder(obj)
        .then(data => {
            sendNewOrder(socket, obj)
            return res.send(JSON.stringify({ STATUS: 1, DATA: obj }));
        })
        .catch(err => {
            console.log(err.toString());
            addLogToList({ folder: "buyorder", text: `Error - addOder=${err.toString()} - ID=${ID}` });
            return res.send(JSON.stringify({ STATUS: 0, NOTE: "Database Error!" }));
        })
}

module.exports = buyorder;