const express = require('express');
const router = express.Router();

router.get("/admin", (req, res) => {
    return res.render("ADMIN")
})

router.get("*", (req, res) => {
    return res.render("UI")
})

/////////////////////// POST ////////////////////////////////////

router.post("/buyorder", require("./post/buyorder.js"));

router.post("/sellorder", require("./post/sellorder.js"))

router.post("/inputBankingAPI", require("./post/inputBankingAPI.js"))

module.exports = router;