const express = require('express');
let { findBankAccount, addBankHistory } = require("../controller/Firebase.js");

const app = express();

const cookieParser = require('cookie-parser');

const cors = require('cors');
const { json } = require('body-parser');
app.use(cookieParser());
app.use(json());
app.use(cors());

app.listen(3000, () => console.log("Baking API start!"));

app.get('/checkAccount/:id', (req, res) => {
    let { id } = req.params;
    if (!id || id.length < 8 || isNaN(id)) return res.send(JSON.stringify({ STATUS: 0, NOTE: "WRONG ID" }))
    return findBankAccount(id)
        .then(idResult => {
            if (idResult) {
                return res.send(JSON.stringify({ STATUS: 1, ID: id, FULLNAME: idResult.FULLNAME }));
            }
            return res.send(JSON.stringify({ STATUS: 0, NOTE: "ACCOUNT ARE NOT EXIST" }))
        })
        .catch(err => {
            console.log(err.toString());
            return res.send(JSON.stringify({ STATUS: 0, NOTE: "CHECK ACCOUNT ERROR" }))
        })
});

app.post("/transfer", (req, res) => {
    let { from, to, value, note } = req.body;
    return addBankHistory(from, to, value, note)
        .then(() => {
            return res.send(JSON.stringify({ STATUS: 1 }));
        })
        .catch(err => {
            console.log(err.toString());
            return res.send(JSON.stringify({ STATUS: 0 }));
        })
})



