import ReactDOM from 'react-dom';
import React from 'react';

import { Provider } from "react-redux";

import store from "./ADMIN-redux.js"

import '../public/ADMIN-styles.css';
import '../public/bootstrap/js/bootstrap.min.js';
import '../public/bootstrap/css/bootstrap.min.css';
import '../public/bootstrap/css/bootstrap-theme.min.css';

import Root from "./Components/ADMIN/Root.jsx";

ReactDOM.render(
    <Provider store={store}>
        <Root />
    </Provider>,
    document.getElementById('root')
);