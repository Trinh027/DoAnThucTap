import ReactDOM from 'react-dom';
import React from 'react';

import { Provider } from "react-redux";

import store from "./redux.js"

import '../public/styles.css';
import '../public/bootstrap/js/bootstrap.min.js';
import '../public/bootstrap/css/bootstrap.min.css';
import '../public/bootstrap/css/bootstrap-theme.min.css';

import Root from "./Components/UI/Root.jsx";

let config = {
    apiKey: "AIzaSyA7859S_4XD4UdtKbPYrXYGplBuXgC0FWg",
    authDomain: "muaban-coin.firebaseapp.com",
    databaseURL: "https://muaban-coin.firebaseio.com",
    projectId: "muaban-coin",
    storageBucket: "muaban-coin.appspot.com",
    messagingSenderId: "194060261604"
};

firebase.initializeApp(config);


ReactDOM.render(
    <Provider store={store}>
        <Root

        />
    </Provider>,
    document.getElementById('root')
);