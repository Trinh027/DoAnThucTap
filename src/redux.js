import { createStore } from 'redux'

let oldstate = {
    list: [],
    Mcoin: {}
};
let Root, Order, HB, Chat;

function todoApp(state = oldstate, action) {
    switch (action.type) {
        case "ADD_ROOT": {
            let temp = state;
            Root = action.here;
            return temp;
        }
        case "ADD_ORDER": {
            let temp = state;
            Order = action.here;
            return temp;
        }
        case "ADD_USER": {
            let temp = state;
            if (Root) {
                Root.adduser(action.user)
            }
            return temp;
        }
        case "ADD_CHAT": {
            let temp = state;
            Chat = action.here;
            return temp;
        }
        case "ADD_HB": {
            let temp = state;
            HB = action.here;
            return temp;
        }
        case "EDIT_HB": {
            let temp = state;
            if (HB) {
                HB.editResponse(action.data)
            }
            return temp;
        }
        case "SET_COIN": {
            let temp = state;
            if (Miner) {
                Miner.setCOIN(action.obj)
            }
            return temp;
        }
        case "SET_USER": {
            let temp = state;
            if (Root) {
                Root.setUser(action.user, action.cb)
            }
            return temp;
        }
        case "SET_DATA": {
            let temp = state;
            if (Root) {
                Root.setdata(action.data, action.cb)
            }
            return temp;
        }
        case "RETURN_LANG": {
            let temp = state;
            if (Root) {
                let tam = Object.assign({}, Root.state.data);
                tam.LANG = Root.props.data.LANG
                Root.setdata(tam)
            }
            return temp;
        }
        case "SET_SIZE": {
            let temp = state;
            if (Root) {
                Root.style_sheet()
            }
            return temp;
        }
        case "EMIT": {
            let temp = state;
            if (Root) {
                Root.socketEmit(action.connect, action.data, action.cb)
            }
            return temp;
        }
        case "RENDER_CHART": {
            let temp = state;
            if (Root) {
                Root.setChart(action.ctx, action.option, action.cb)
            }
            return temp;
        }
        case "CHANGE_PRICE": {
            let temp = state;
            if (Order) {
                Order.amountChange("buy");
                Order.amountChange("sell");
            }
            return temp;
        }
        case "ORDER_DETAIL": {
            let temp = state;
            if (Order) {
                Order.openOrderDetail(action.data)
            }
            return temp;
        }
        case "CHECKING_BANKACCOUNT_RESPONSE": {
            let temp = state;
            if (Order) {
                Order.alertBankAccountResponse(action.data);
            }
            return temp;
        }
        case "MOVE_CHAT_TO_BOTTOM": {
            let temp = state;
            if (Chat) {
                Chat.moveToBottom(action.flat);
            }
            return temp;
        }
    }

}

let store = createStore(todoApp);

module.exports = store; 
