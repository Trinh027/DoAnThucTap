import { createStore } from 'redux'

let oldstate = {
    list: [],
    Mcoin: {}
};
let Root, System, Chat, User, Massage;

function todoApp(state = oldstate, action) {
    switch (action.type) {
        case "ADD_ROOT": {
            let temp = state;
            Root = action.here;
            return temp;
        }
        case "ADD_USER": {
            let temp = state;
            User = action.here;
            return temp;
        }
        case "ADD_MASSAGE": {
            let temp = state;
            Massage = action.here;
            return temp;
        }
        case "ADD_SYSTEM": {
            let temp = state;
            System = action.here;
            return temp;
        }
        case "ADD_CHAT": {
            let temp = state;
            Chat = action.here;
            return temp;
        }
        case "EMIT": {
            let temp = state;
            if (Root) {
                Root.socketEmit(action.connect, action.data, action.cb)
            }
            return temp;
        }
        case "SET_SELECT": {
            let temp = state;
            if (Root) {
                Root.setSelect(action.select)
            }
            return temp;
        }
        case "RESET_SYSTEM": {
            let temp = state;
            if (System) {
                System.resetState();
            }
            return temp;
        }
        case "RELEASE_EDIT_CLICK": {
            let temp = state;
            if (System) {
                System.releaseEditClick();
            }
            return temp;
        }
        case "MOVE_CHAT_TO_BOTTOM": {
            let temp = state;
            if (Chat) {
                Chat.moveToBottom();
            }
            return temp;
        }
        case "MOVE_MASSAGE_TO_BOTTOM": {
            let temp = state;
            if (Massage) {
                Massage.moveToBottom();
            }
            return temp;
        }
        case "SHOW_USER_LIST": {
            let temp = state;
            if (User) {
                User.showListUser(action.list);
            }
            return temp;
        }
        case "REMOVE_UNREADLIST": {
            let temp = state;
            if (User) {
                User.removeUnReadList(action.ID);
            }
            return temp;
        }
        case "REMOVE_MASSAGELIST": {
            let temp = state;
            if (Root) {
                Root.removeMassageList(action.ID);
            }
            return temp;
        }
    }

}

let store = createStore(todoApp);

module.exports = store; 
