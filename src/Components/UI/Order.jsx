import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";

import ModalBody from "./ModalBody.jsx";

const isValid = wallet => require('bitcore-lib').Address.isValid(wallet, 'livenet');

let here;


export class Order extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            buyWalletAlert: "",
            buyAmountAlert: "",
            sellAmountAlert: "",
            bankAccountAlert: "",
            accountOwner: "",
            RESULT: null
        };
        here.props.dispatch({
            type: "ADD_ORDER",
            here: here
        })
    }
    formatNum(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    walletChange() {
        let tam = here.refs.buyWallet.value.trim();
        if (tam.length == 34 && tam.length == tam.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) {
            if (isValid(tam)) {
                here.setState({ buyWalletAlert: "success" })
            } else {
                here.setState({ buyWalletAlert: "error" })
            }
        } else {
            if (tam.length > 0) {
                here.setState({ buyWalletAlert: "error" })
            } else {
                here.setState({ buyWalletAlert: "" })
            }
        }
    }
    amountChange(ref) {
        here.refs[`${ref}Amount`].value = here.refs[`${ref}Amount`].value.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]/g, '');
        let tam = here.refs[`${ref}Amount`].value.trim();
        if (isNaN(tam)) {
            here.setState({ [`${ref}AmountAlert`]: "error" });
        } else {
            if (tam.indexOf(".") > -1 && tam.split(".")[1].length > 8) {
                tam = parseInt(tam * 100000000) / 100000000;
                here.refs[`${ref}Amount`].value = tam;
            }
            here.setState({ [`${ref}AmountAlert`]: "" }, () => {
                let total = Math.round(tam * here.props.DATA[`${ref == "buy" ? "BUY" : "SELL"}PRICE`]);
                here.refs[`${ref}Total`].value = total ? here.formatNum(total) : null;
            })
        }
    }
    sendBuyOrder() {
        if (!here.props.USER.ID) {
            return alert("Please Login To Order!");
        }
        if (here.state.buyWalletAlert != "success") {
            return alert("Please re-check your Wallet!");
        }
        if (here.state.buyAmountAlert != "" || !parseFloat(here.refs.buyAmount.value) || parseFloat(here.refs.buyAmount.value) <= 0) {
            return alert("Please re-check your Amount of BTC!");
        }
        let body = {
            ID: here.props.USER.ID,
            SOCKET: here.props.SOCKET.id,
            WALLET: here.refs.buyWallet.value,
            AMOUNT: here.refs.buyAmount.value
        };
        return fetch('/buyorder', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                if (data.STATUS == 1) {
                    here.setState({ RESULT: data.DATA }, () => $("#orderDetail").modal("show"))
                } else {
                    alert(data.NOTE)
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
    sendSellOrder() {
        if (!here.props.USER.ID) {
            return alert("Please Login To Order!");
        }
        if (here.state.sellAmountAlert != "" || !parseFloat(here.refs.sellAmount.value) || parseFloat(here.refs.sellAmount.value) <= 0) {
            return alert("Please re-check Amount Of BTC!");
        }
        if (here.state.bankAccountAlert != "success") {
            return alert("Please re-check your Bank Account!");
        }
        let body = {
            ID: here.props.USER.ID,
            SOCKET: here.props.SOCKET.id,
            ACCOUNT: here.refs.bankAccount.value,
            AMOUNT: here.refs.sellAmount.value,
            ACCOUNTOWNER: here.refs.accountOwner.value
        };
        return fetch('/sellorder', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                if (data.STATUS == 1) {
                    return here.openOrderDetail(data.DATA)
                } else {
                    alert(data.NOTE)
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
    openOrderDetail(data) {
        return here.setState({ RESULT: data }, () => $("#orderDetail").modal("show"))
    }
    checkBankAcount() {
        here.refs.bankAccount.value = here.refs.bankAccount.value.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]/g, '');
        let temp = here.refs.bankAccount.value;
        let length = temp.length;
        if (length == 0) {
            here.refs.accountOwner.value = "";
            return here.setState({ bankAccountAlert: "" });
        }
        if (length > 0 && length < 9) {
            here.refs.accountOwner.value = "";
            return here.setState({ bankAccountAlert: "error" });
        }
        return here.props.dispatch({
            type: "EMIT",
            connect: "CHECKING_BANKACCOUNT_REQUEST",
            data: { ID: temp }
        })
    }
    alertBankAccountResponse(data) {
        let temp = here.refs.bankAccount.value;
        if (temp == data.ID && data.STATUS) {
            here.refs.accountOwner.value = data.FULLNAME;
            return here.setState({
                bankAccountAlert: data.STATUS == 2 ? "error" : "success"
            })
        }
        if (temp == data.ID && !data.STATUS) {
            here.refs.accountOwner.value = "";
            return here.setState({
                bankAccountAlert: "error"
            })
        }
    }
    render() {
        return (
            <div className="row order_zone">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div className="caculator">
                        <div className="title">Buy</div>
                        <div id="buyPrice" className="price">{here.formatNum(here.props.DATA.BUYPRICE)} VND</div>
                        <div className="input_group">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><i className="fas fa-wallet"></i></span>
                                </div>
                                <input ref="buyWallet" type="text" className={`form-control ${here.state.buyWalletAlert}`} placeholder="Vui lòng nhập vào ví nhận BTC" maxLength="34" minLength="34" onInput={here.walletChange} />
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><i className="fab fa-bitcoin"></i></span>
                                </div>
                                <input ref="buyAmount" type="text" className={`form-control ${here.state.buyAmountAlert}`} placeholder="Vui lòng nhập vào số lượng BTC muốn mua" onInput={() => here.amountChange("buy")} />
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><i className="fas fa-dollar-sign"></i></span>
                                </div>
                                <input ref="buyTotal" disabled type="text" className="form-control" placeholder="Tổng tiền phải trả" />
                            </div>
                            <div className="order_button">
                                <button className="btn btn-success" onClick={here.sendBuyOrder} >Buy</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div className="caculator">
                        <div className="title">Sell</div>
                        <div id="sellPrice" className="price">{here.formatNum(here.props.DATA.SELLPRICE)} VND</div>
                        <div className="input_group">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><i className="fas fa-university"></i></span>
                                </div>
                                <input ref="bankAccount" type="text" className={`form-control ${here.state.bankAccountAlert}`} placeholder="Vui lòng nhập vào tài khoản nhận tiền" onInput={here.checkBankAcount} maxLength="9" />
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><i className="fas fa-user"></i></span>
                                </div>
                                <input disabled ref="accountOwner" type="text" className="form-control" placeholder="Tên chủ tài khoản (Dùng 123456789 để kiểm tra)" />
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><i className="fab fa-bitcoin"></i></span>
                                </div>
                                <input ref="sellAmount" type="text" className={`form-control ${here.state.sellAmountAlert}`} placeholder="Vui lòng nhập vào số lượng BTC muốn bán" onInput={() => here.amountChange("sell")} />
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><i className="fas fa-dollar-sign"></i></span>
                                </div>
                                <input ref="sellTotal" disabled type="text" className="form-control" placeholder="Tổng tiền sẽ nhận" />
                            </div>
                            <div className="order_button">
                                <button className="btn btn-success" onClick={here.sendSellOrder} >Sell</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="orderDetail" className="modal" role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Order Details</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <ModalBody
                                RESULT={here.state.RESULT}
                            />
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    componentDidMount() {

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(Order);