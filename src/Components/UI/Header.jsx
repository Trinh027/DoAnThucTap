import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";

import HeaderButton from "./HeaderButton.jsx";

let here;

export class Header extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    render() {
        return (
            <div className="row header">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 logo">
                    <a href="/"><img id="logo" src="Images/sitelogo.png" alt="btc bitcoin mining etc ethereum" /></a>
                </div>
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <HeaderButton
                        USER={here.props.USER}
                        DATA={here.props.DATA}
                    />
                </div>
            </div >
        );
    }
    componentDidMount() {
        $('#wall-slideshow').carousel({
            interval: 4000,
            wrap: true,
            pause: false
        })

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(Header);