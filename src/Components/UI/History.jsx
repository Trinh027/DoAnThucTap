import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";

let here;

export class History extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    formatDay(time) {
        let now = new Date().getTime();
        let past = new Date(time).getTime();
        let giay = parseInt((now - past) / 1000);
        if (giay < 60) {
            return `${giay} giây trước`;
        } else if (giay < 3600) {
            return `${parseInt(giay / 60)} phút trước`;
        } else if (giay < 86400) {
            return `${parseInt(giay / 3600)} giờ trước`;
        } else if (giay < 604800) {
            return `${parseInt(giay / 86400)} ngày trước`;
        } else if (giay < 2419200) {
            return `${parseInt(giay / 604800)} tuần trước`;
        }
    }
    formatNum(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    openDetail(data) {
        return here.props.dispatch({
            type: "ORDER_DETAIL",
            data
        })
    }
    render() {
        if (here.props.USER.ID) {
            if (here.props.USER.HISTORY && here.props.USER.HISTORY.length > 0) {
                return (
                    <div className="row payment" >
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="table-responsive">
                                <table className="table table-hover table-striped">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th scope="col">Type</th>
                                            <th scope="col">Time</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Out</th>
                                            <th scope="col">Status</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {here.props.USER.HISTORY.map((e, i) => {
                                            let time = here.formatDay(e.CREATETIME)
                                            let amount = (e.TYPE == "BUY") ? `${e.AMOUNT} BTC` : `${here.formatNum(e.TOTAL)} VND`;
                                            let output = (e.TYPE == "BUY") ? e.WALLET : e.ACCOUNT;
                                            let status = "Wating";
                                            if (e.STATUS == 1) {
                                                status = "Working";
                                            } else if (e.STATUS == 2) {
                                                status = "Done";
                                            } else if (e.STATUS == 3) {
                                                status = "Cancel";
                                            }
                                            return (
                                                <tr key={i}>
                                                    <td>{e.TYPE}</td>
                                                    <td>{time}</td>
                                                    <td>{amount}</td>
                                                    <td>{output}</td>
                                                    <td>{status}</td>
                                                    <td style={{ cursor: "pointer", color: "#007bff" }} onClick={() => here.openDetail(e)} >detail</td>
                                                </tr>
                                            );

                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="row payment" >
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="table-responsive">
                                <table className="table table-hover table-striped">
                                    <thead className="thead-dark">
                                        <tr>
                                            <th scope="col">Type</th>
                                            <th scope="col">Time</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Wallet</th>
                                            <th scope="col">Status</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                );
            }
        } else {
            return (
                <div></div>
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(History);