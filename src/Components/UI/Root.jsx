import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";
import ck from "js-cookie";
import io from "socket.io-client";

import Header from "./Header.jsx";
import Order from "./Order.jsx";
import Chat from "./Chat.jsx";
import History from "./History.jsx";

import Chart from 'chart.js';

let here;

export class Root extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            USER: { UID: "0" },
            SOCKET: io(),
            DATA: {
                SELLPRICE: 0,
                BUYPRICE: 0,
                TRANSFERFEE: 1
            },
            CHAT: {
                PUBLIC: [],
                PRIVATE: []
            }
        }
        if (!ck.getJSON("LANGUAGE")) {
            ck.set("LANGUAGE", "English");
        }
        let { dispatch } = here.props;
        dispatch({
            type: "ADD_ROOT",
            here: here
        })
    }
    setUser(user, cb) {
        return here.setState({ USER: user }, () => {
            if (cb) cb()
        })
    }
    setChart(ctx, option, cb) {
        Chart.defaults.global.animation.duration = 0;
        here.setState({ myChart: new Chart(ctx, option) }, () => {
            if (cb) cb()
        })
    }
    socketEmit(connect, data, cb) {
        here.state.SOCKET.emit(connect, JSON.stringify(data));
        if (cb) return cb;
        return 1;
    }
    render() {
        return (
            <div>
                <div className="container-fluid" >

                    <Header
                        USER={here.state.USER}
                        DATA={here.state.DATA}
                    />

                    <Order
                        DATA={here.state.DATA}
                        SOCKET={here.state.SOCKET}
                        USER={here.state.USER}
                    />
                    <History
                        DATA={here.state.DATA}
                        USER={here.state.USER}
                    />
                    <Chat
                        show={parseInt(ck.get("CHATBOX"))}
                        USER={here.state.USER}
                        CHAT={here.state.CHAT}
                    />
                </div>
            </div>
        );
    }
    componentDidMount() {
        here.state.SOCKET.emit("RESULT_REQUEST");
        here.state.SOCKET.on("RESULT_RESPONSE", data => {
            data = JSON.parse(data);
            here.setState({ DATA: data }, () => {
                here.state.SOCKET.on("UPDATE_PRICE", data => {
                    data = JSON.parse(data);
                    let tam = Object.assign({}, here.state.DATA);
                    tam[data.TYPE] = parseFloat(data.VALUE);
                    if (data.TYPE == "BUYPRICE") {
                        if (data.VALUE > here.state.DATA.BUYPRICE) {
                            $("#buyPrice").addClass("up");
                            setTimeout(() => {
                                $("#buyPrice").removeClass("up");
                            }, 500)
                        } else if (data.VALUE < here.state.DATA.BUYPRICE) {
                            $("#buyPrice").addClass("down");
                            setTimeout(() => {
                                $("#buyPrice").removeClass("down");
                            }, 500)
                        }
                    } else {
                        if (data.VALUE > here.state.DATA.SELLPRICE) {
                            $("#sellPrice").addClass("up");
                            setTimeout(() => {
                                $("#sellPrice").removeClass("up");
                            }, 500)
                        } else if (data.VALUE < here.state.DATA.SELLPRICE) {
                            $("#sellPrice").addClass("down");
                            setTimeout(() => {
                                $("#sellPrice").removeClass("down");
                            }, 500)
                        }
                    }
                    here.setState({ DATA: tam }, () => {
                        let { dispatch } = here.props;
                        dispatch({
                            type: "CHANGE_PRICE"
                        })
                    });
                })
            })
        });
        here.state.SOCKET.on("LOGIN_RESPONSE", data => {
            data = JSON.parse(data);
            if (data.STATUS == 1) {
                here.setState({ USER: data.USER }, () => {
                    here.socketEmit("HISTORY_REQUEST", { ID: here.state.USER.ID, amount: 10 })
                });
            } else {
                alert(data.NOTE);
            }
        });
        here.state.SOCKET.on("EDIT_RESPONSE", data => {
            JSON.parse(data);
            if (data.STATUS) {
                let user = Object.assign({}, here.state.USER);
                for (let key in data.USER) {
                    user[key] = data.USER[key];
                }
                here.setState({ USER: user }, () => {
                    let { dispatch } = here.props;
                    dispatch({
                        type: "EDIT_HB",
                        data
                    })
                })
            } else {
                let { dispatch } = here.props;
                dispatch({
                    type: "EDIT_HB",
                    data
                })
            }
        });
        here.socketEmit("PUBLIC_CHAT_REQUEST");
        here.state.SOCKET.on("PUBLIC_CHAT_RESPONSE", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.CHAT);
            tam.PUBLIC = data;
            here.setState({ CHAT: tam }, () => {
                return here.props.dispatch({
                    type: "MOVE_CHAT_TO_BOTTOM",
                    flat: true
                })
            })
        });
        here.state.SOCKET.on("SEND_PUBLICCHAT", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.CHAT);
            tam.PUBLIC.push(data);
            here.setState({ CHAT: tam }, () => {
                return here.props.dispatch({
                    type: "MOVE_CHAT_TO_BOTTOM",
                    flat: true
                })
            })
        });
        here.state.SOCKET.on("CHECKING_BANKACCOUNT_RESPONSE", data => {
            data = JSON.parse(data);
            here.props.dispatch({
                type: "CHECKING_BANKACCOUNT_RESPONSE",
                data: data
            })
        })
        here.state.SOCKET.on("HISTORY_RESPONSE", data => {
            data = JSON.parse(data);
            let temp = Object.assign({}, here.state.USER);
            temp.HISTORY = data.HISTORY;
            here.setState({
                USER: temp
            })
        })
        here.state.SOCKET.on("NEW_ORDER", data => {
            data = JSON.parse(data);
            let temp = Object.assign({}, here.state.USER);
            temp.HISTORY = [data].concat(temp.HISTORY);
            here.setState({ USER: temp });
        })
        here.state.SOCKET.on("PRIVATE_CHAT_RESPONSE", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.CHAT);
            tam.PRIVATE = data;
            here.setState({ CHAT: tam }, () => {
                return here.props.dispatch({
                    type: "MOVE_CHAT_TO_BOTTOM",
                    flat: false
                })
            })
        });
        here.state.SOCKET.on("SEND_PRIVATECHAT", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.CHAT);
            tam.PRIVATE.push(data);
            here.setState({ CHAT: tam }, () => {
                return here.props.dispatch({
                    type: "MOVE_CHAT_TO_BOTTOM",
                    flat: false
                })
            })
        });
        here.state.SOCKET.on("UPDATE_HISTORY", data => {
            data = JSON.parse(data);
            console.log(data)
            let tam = Object.assign({}, here.state.USER);
            let length = tam.HISTORY.length;
            for (let i = 0; i < length; i++) {
                if (data.CREATETIME == tam.HISTORY[i].CREATETIME) {
                    tam.HISTORY[i].STATUS = data.STATUS;
                    break;
                }
            }
            return here.setState({ USER: tam });
        });
        here.state.SOCKET.on("TRANSFER_BTC_RESPONSE", data => {
            data = JSON.parse(data);
            return alert(data.NOTE)
        })
    }
};

module.exports = connect((state) => ({ mang: state }))(Root);