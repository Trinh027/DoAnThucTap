import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";

let here;

export class ChatBox extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    formatDay(time) {
        let now = new Date().getTime();
        let past = new Date(time).getTime();
        let giay = parseInt((now - past) / 1000);
        if (giay < 60) {
            return `${giay} giây trước`;
        } else if (giay < 3600) {
            return `${parseInt(giay / 60)} phút trước`;
        } else if (giay < 86400) {
            return `${parseInt(giay / 3600)} giờ trước`;
        } else if (giay < 604800) {
            return `${parseInt(giay / 86400)} ngày trước`;
        } else if (giay < 2419200) {
            return `${parseInt(giay / 604800)} tuần trước`;
        }
    }
    render() {
        if (here.props.public) {
            return here.props.CHAT.PUBLIC.map((e, i) => {
                let color = "gray";
                if (e.STATUS == 1) {
                    color = "red";
                }
                if (here.props.USER.ID == e.ID) {
                    return (
                        <li key={i} className="chat_inner yourself">
                            <div className="content">{e.CONTENT}</div>
                            <div className="info">
                                <span className="nickname" style={{ color }}>{e.NICKNAME} - {here.formatDay(e.TIME)}</span>
                            </div>
                        </li>
                    );
                } else {
                    return (
                        <li key={i} className="chat_inner">
                            <div className="content">{e.CONTENT}</div>
                            <div className="info">
                                <span className="nickname" style={{ color }}>{e.NICKNAME} - {here.formatDay(e.TIME)}</span>
                            </div>
                        </li>
                    );
                }
            })
        } else {
            return here.props.CHAT.PRIVATE.map((e, i) => {
                if (here.props.USER.ID == e.SENDPERSON) {
                    return (
                        <li key={i} className="chat_inner yourself">
                            <div className="content">{e.CONTENT}</div>
                            <div className="info">
                                <span className="nickname">You - {here.formatDay(e.TIME)}</span>
                            </div>
                        </li>
                    );
                } else {
                    return (
                        <li key={i} className="chat_inner">
                            <div className="content">{e.CONTENT}</div>
                            <div className="info">
                                <span className="nickname">Admin - {here.formatDay(e.TIME)}</span>
                            </div>
                        </li>
                    );
                }
            })
        }
    }
    componentDidMount() {

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(ChatBox);