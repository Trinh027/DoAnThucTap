import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";

let here;

export class ModalBody extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    formatNum(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        if (here.props.RESULT) {
            let t = new Date(here.props.RESULT.CREATETIME);
            let time = `${t.getUTCDate()}/${t.getUTCMonth() + 1}/${t.getUTCFullYear()}`;
            let total = here.formatNum(here.props.RESULT.TOTAL);
            if (here.props.RESULT.TYPE == "BUY") {
                return (
                    <div className="modal-body" >
                        <div className="type">{here.props.RESULT.TYPE}</div>
                        <div className="line">Please send <strong>{total} VND</strong> to this account <strong>{here.props.RESULT.ACCOUNT}</strong> </div>
                        <div className="line">
                            SAMPLE: <br />
                            ACCOUNT: {here.props.RESULT.ACCOUNT} <br />
                            MONEY: {here.props.RESULT.TOTAL} <br />
                            NOTE: "for {here.props.RESULT.KEY}"
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-key"></i></span>
                            </div>
                            <input type="text" className="form-control" value={here.props.RESULT.KEY} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-user"></i></span>
                            </div>
                            <input type="text" className="form-control" value={here.props.RESULT.ID} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-clock"></i></span>
                            </div>
                            <input type="text" className="form-control" value={time} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-university"></i></span>
                            </div>
                            <input type="text" className="form-control" value={here.props.RESULT.ACCOUNT} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-wallet"></i></span>
                            </div>
                            <input type="text" className={`form-control`} value={here.props.RESULT.WALLET} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fab fa-bitcoin"></i></span>
                            </div>
                            <input type="text" className={`form-control`} value={here.props.RESULT.AMOUNT} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-dollar-sign"></i></span>
                            </div>
                            <input type="text" className="form-control" value={total} />
                        </div>
                    </div >
                );
            } else {
                return (
                    <div className="modal-body" >
                        <div className="type">{here.props.RESULT.TYPE}</div>
                        <div className="line">Please send <strong>{here.props.RESULT.AMOUNT}</strong> BTC to this wallet: <strong>{here.props.RESULT.WALLET}</strong> </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-key"></i></span>
                            </div>
                            <input type="text" className="form-control" value={here.props.RESULT.KEY} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-user"></i></span>
                            </div>
                            <input type="text" className="form-control" value={here.props.RESULT.ID} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-clock"></i></span>
                            </div>
                            <input type="text" className="form-control" value={time} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-wallet"></i></span>
                            </div>
                            <input type="text" className={`form-control`} value={here.props.RESULT.WALLET} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-university"></i></span>
                            </div>
                            <input type="text" className="form-control" value={here.props.RESULT.ACCOUNT} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fab fa-bitcoin"></i></span>
                            </div>
                            <input type="text" className={`form-control`} value={here.props.RESULT.AMOUNT} />
                        </div>
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon1"><i className="fas fa-dollar-sign"></i></span>
                            </div>
                            <input type="text" className="form-control" value={total} />
                        </div>
                    </div >
                );
            }
        } else {
            return (
                <div></div>
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(ModalBody);