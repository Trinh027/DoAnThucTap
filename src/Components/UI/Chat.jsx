import React from 'react';
import ck from "js-cookie";

import store from "../../redux.js"
import { connect } from "react-redux";

import ChatBox from "./ChatBox.jsx";

let here;

export class Chat extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            show: here.props.show,
            public: true
        }
        this.props.dispatch({
            type: "ADD_CHAT",
            here
        })
    }
    changeShow() {
        here.setState({ show: here.state.show ? 0 : 1 }, () => {
            ck.set("CHATBOX", here.state.show);
        });
    }
    moveToBottom(x) {
        if (here.state.public == x) {
            let chatBox = document.getElementById("chatBox")
            if (chatBox) return $("#chatBox").scrollTop(chatBox.offsetHeight * 2);
        }
    }
    changeBox(x) {
        if (here.state.public != x) {
            here.setState({ public: x }, () => {
                here.moveToBottom(here.state.public);
                if (here.state.public == false && here.props.USER.ID && here.props.CHAT.PRIVATE.length == 0) {
                    here.props.dispatch({
                        type: "EMIT",
                        connect: "PRIVATE_CHAT_REQUEST",
                        data: { ID: here.props.USER.ID }
                    })
                }
            });
        }
    }
    sendMessenge() {
        if (!here.props.USER.ID) {
            return alert("Please Login!");
        }
        if (!here.refs.chatInput.value && here.refs.chatInput.value.length == 0) {
            return alert("Please Enter Something!");
        }
        if (here.state.public) {
            let obj = {
                ID: here.props.USER.ID,
                CONTENT: here.refs.chatInput.value.trim(),
                NICKNAME: here.props.USER.NICKNAME
            }
            here.props.dispatch({
                type: "EMIT",
                connect: "PUBLIC_CHAT_SEND",
                data: obj
            })
        } else {
            let obj = {
                ID: here.props.USER.ID,
                CONTENT: here.refs.chatInput.value.trim(),
                NICKNAME: here.props.USER.NICKNAME
            }
            here.props.dispatch({
                type: "EMIT",
                connect: "PRIVATE_CHAT_SEND",
                data: obj
            })
        }
        here.refs.chatInput.value = null;
    }
    writeMassage(event) {
        if (event.key == 'Enter') {
            return here.sendMessenge()
        }
    }
    render() {
        if (here.state.show) {
            return (
                <div className="chat">
                    <div className="chat_setting" onClick={here.changeShow} >
                        Hide ChatBox
                    </div>
                    <div className="chat_option">
                        <div className="chat_option_btn public_chat_btn" onClick={() => here.changeBox(true)} >Public Chat</div>
                        <div className="chat_option_btn private_chat_btn" onClick={() => here.changeBox(false)} >Admin</div>
                    </div>
                    <ul className="chatbox" id="chatBox">
                        <ChatBox
                            public={here.state.public}
                            USER={here.props.USER}
                            CHAT={here.props.CHAT}
                        />
                    </ul>
                    <div className="inputzone">
                        <div className="input-group mb-3">
                            <input ref="chatInput" type="text" className="form-control" placeholder="..." onKeyPress={here.writeMassage} maxLength="200" />
                            <div className="input-group-append">
                                <button className="btn btn-primary" type="button" onClick={here.sendMessenge} >Send</button>
                            </div>
                        </div>
                    </div>
                </div >
            );
        } else {
            return (
                <div className="chat hide" onClick={here.changeShow}>
                    <div className="chat_setting">
                        <i className="far fa-comment-alt"></i>
                    </div>
                </div >
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(Chat);