import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";
import ck from "js-cookie";

let here;

const isPrivateKey = pk => require('bitcore-lib').PrivateKey.isValid(pk, 'livenet');
const isWallet = wallet => require('bitcore-lib').Address.isValid(wallet, 'livenet');

let REGEX = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
let Emailcheck = E => (typeof E === 'string' && E.length > 5 && E.length < 30 && REGEX.test(E)) ? E.toLowerCase() : false;

export class HeaderButton extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            edit_click: 1,
            email_alert: "",
            edit_alert_class: "",
            edit_alert_inner: "",
            nickname_arlert: "",
            from_wallet_alert: "",
            to_wallet_alert: "",
            transferPK_alert: ""
        }
        here.props.dispatch({
            type: "ADD_HB",
            here
        })
    }
    login(str) {
        if (str == "fb") {
            return here.loginhand((new firebase.auth.FacebookAuthProvider()));
        }
        return here.loginhand((new firebase.auth.GoogleAuthProvider()));
    }
    Popup(pr) {
        return firebase.auth().signInWithPopup(pr);
    }
    loginhand(pr) {
        here.Popup(pr)
            .then(user => {
                let { email, phoneNumber } = user.user;
                let { providerId, uid } = user.user.providerData['0'];
                let time = new Date().getTime();
                let obj = { email, phoneNumber, uid, providerId }
                here.update_user(obj)
            })
            .catch(error => {
                alert(error.toString())
            });
    }
    update_user(obj) {
        return firebase.auth().currentUser.getIdToken()
            .then(token => {
                obj.token = token;
                obj.AS = ck.get("AS");
                obj.USIP = window.navigator.userAgent;
                return here.props.dispatch({
                    type: "EMIT",
                    connect: "LOGIN_REQUEST",
                    data: obj
                })
            })
            .catch(err => alert(err.toString()))
    }
    email_change() {
        if (here.refs.email.value) {
            let tam = here.refs.email.value.trim()
            let a = Emailcheck(tam)
            if (a) {
                here.refs.email.value = tam.toLowerCase();
                here.setState({
                    email_alert: "success"
                })
            } else {
                here.setState({
                    email_alert: "error"
                })
            }
        } else {
            here.setState({
                email_alert: ""
            })
        }
    }
    phone_change() {
        let tam = here.refs.phone.value;
        if (tam.length !== tam.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) {
            here.refs.phone.value = tam.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
        }
    }
    render_edit_button() {
        if (!here.state.edit_click) {
            return (
                <div className="modal-footer">
                    <button disabled type="button" className="btn btn-primary" > <img src="Images/loading.gif" /></button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal" >Close</button>
                </div>
            );
        } else {
            return (
                <div className="modal-footer">
                    <button type="button" className="btn btn-primary" onClick={here.editclick} >Save</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal" >Close</button>
                </div>
            );
        }
    }
    render_edit_alert() {
        if (!here.state.edit_click) {
            here.refs.alert_edit.style.display = "none"
            return (
                <div></div>
            );
        } else {
            return (
                <div className={`alert${here.state.edit_alert_class}`} role="alert">
                    {here.state.edit_alert_inner}
                </div>
            );
        }
    }
    editclick() {
        let edit = 0;
        if (here.state.edit_click) {
            here.setState({ edit_click: false }, () => {
                let tam = {}
                if (here.refs.email.value && here.refs.email.value.length > 0) {
                    if (here.state.email_alert === "success") {
                        edit = 1;
                        tam.email = here.refs.email.value
                    } else {
                        here.setState({ edit_click: true });
                        return alert("Email error")
                    }
                }
                if (here.refs.nickname.value && here.refs.nickname.value.length > 0) {
                    edit = 1;
                    tam.nickname = here.refs.nickname.value
                }
                if (here.refs.phone.value && here.refs.phone.value.length > 0) {
                    edit = 1;
                    tam.phone = here.refs.phone.value
                }
                if (edit) {
                    tam.ID = here.props.USER.ID;
                    here.props.dispatch({
                        type: "EMIT",
                        connect: "EDIT_USER",
                        data: tam
                    })
                } else {
                    here.setState({ edit_click: true });
                    alert("You must change something")
                }
            })
        } else {
            here.setState({ edit_click: true });
            alert("You must wait...")
        }
    }
    changeWallet(ref) {
        if (!here.refs[ref]) return
        here.refs[ref].value = here.refs[ref].value.replace(/[ !@#$%^&*()_+\-=\[\]{};':`"\\|,.<>\/?]/g, '');
        let tam = here.refs[ref].value;
        if (tam.length == 0) return here.setState({ [`${ref}_alert`]: "" });
        if (tam.length != 34) return here.setState({ [`${ref}_alert`]: " error" });
        if (!isWallet(tam)) return here.setState({ [`${ref}_alert`]: " error" });
        return here.setState({ [`${ref}_alert`]: " success" });
    }
    changePK(ref) {
        if (!here.refs[ref]) return
        here.refs[ref].value = here.refs[ref].value.replace(/[ !@#$%^&*()_+\-=\[\]{};':`"\\|,.<>\/?]/g, '');
        let tam = here.refs[ref].value;
        if (tam.length == 0) return here.setState({ [`${ref}_alert`]: "" });
        if (tam.length != 64) return here.setState({ [`${ref}_alert`]: " error" });
        if (!isPrivateKey(tam)) return here.setState({ [`${ref}_alert`]: " error" });
        return here.setState({ [`${ref}_alert`]: " success" });
    }
    editResponse(data) {
        data = JSON.parse(data);
        if (data.STATUS) {
            here.setState({
                edit_click: true,
                email_alert: ""
            });
            alert("Edit Complete!");
        } else {
            here.setState({ edit_click: true });
            alert(data.NOTE)
        }
    }
    nickname_change() {
        here.refs.nickname.value = here.refs.nickname.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '')
        let tam = here.refs.nickname.value;
        let length = tam.length;
        if (length == 0) {
            return here.setState({ nickname_arlert: "" });
        }
        if ((length < 4 && length > 0) || length > 12) {
            return here.setState({ nickname_arlert: "error" });
        }
        if (length > 3 && length < 13 && tam == here.props.USER.NICKNAME) {
            return here.setState({ nickname_arlert: "error" });
        }
        return here.setState({ nickname_arlert: "success" });
    }
    openSetting() {
        $("#setting_modal").modal("show");
    }
    logout() {
        location.reload();
    }
    transferBTC() {
        if (here.state.from_wallet_alert != " success") return alert("Please re-check Your Wallet")
        if (here.state.to_wallet_alert != " success") return alert("Please re-check Receive Wallet")
        if (here.state.transferPK_alert != " success") return alert("Please re-check Your Privatekey")
        // if (isNaN(here.refs.transferAmount.value)) return alert("Please re-check Amount")
        return here.props.dispatch({
            type: "EMIT",
            connect: "TRANSFER_BTC_REQUEST",
            data: {
                from: here.refs.from_wallet.value,
                to: here.refs.to_wallet.value,
                value: here.refs.transferAmount.value,
                privatekey: here.refs.transferPK.value
            }
        })
    }
    openTransferModal() {
        return here.setState({ balance: -1 }, () => {
            $("#transferBTC_modal").modal("show");
        })
    }
    amountChange() {
        if (!here.refs.transferAmount) return;
        here.refs.transferAmount.value = here.refs.transferAmount.value.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};'`:"\\|,<>\/?]/g, '');
        let tam = here.refs.transferAmount.value.trim();
        if (tam.length > 0) {
            if (tam.indexOf(".") > -1 && tam.split(".")[1].length > 8) {
                tam = parseInt(tam * 100000000) / 100000000;
                here.refs.transferAmount.value = tam;
            }
        }
    }
    render() {
        if (here.props.USER && here.props.USER.ID) {
            return (
                <div>
                    <div className="user_button">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <button className="btn btn-primary" onClick={() => here.openSetting()} >Setting</button>
                            <button className="btn btn-primary" onClick={here.logout}  >Logout</button>
                            <button className="btn btn-primary" onClick={here.openTransferModal} >TransferBTC</button>
                        </div>
                    </div>
                    <div className="modal fade" id="transferBTC_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">Transfer Bitcoin</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="input-group mb-3">
                                        <input ref="from_wallet" type="text" className={`form-control${here.state.from_wallet_alert}`} placeholder="Please Enter Your Wallet" maxLength="34" onInput={() => here.changeWallet("from_wallet")} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input ref="to_wallet" type="text" className={`form-control${here.state.to_wallet_alert}`} placeholder="Please Enter Receive Wallet" maxLength="34" onInput={() => here.changeWallet("to_wallet")} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input ref="transferAmount" type="text" className={`form-control`} placeholder="Please Transfer Amount Of Bitcoin" onInput={here.amountChange} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input ref="transferPK" type="text" className={`form-control${here.state.transferPK_alert}`} placeholder="Please Enter Privatekey Of Your Wallet" maxLength="64" onInput={() => here.changePK("transferPK")} />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={here.transferBTC} >Send</button>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal fade" id="setting_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">Account Setting</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <label >ID</label>
                                    <div className="input-group mb-3">
                                        <input readOnly type="text" className="form-control" aria-describedby="basic-addon3" defaultValue={`${here.props.USER.ID}`} />
                                    </div>
                                    <label>Nickname</label>
                                    <div className="input-group mb-3">
                                        <input type="text" ref="nickname" className={`form-control ${here.state.nickname_arlert}`} placeholder={here.props.USER.NICKNAME ? here.props.USER.NICKNAME : "Please enter your nickname (6-12 charactors)"} onChange={here.nickname_change} maxLength="12" />
                                    </div>
                                    <label>Email</label>
                                    <div className="input-group mb-3">
                                        <input type="text" ref="email" className={`form-control ${here.state.email_alert}`} placeholder={here.props.USER.EMAIL ? here.props.USER.EMAIL : "Please enter your email"} onChange={here.email_change} />
                                    </div>
                                    <label >Phone</label>
                                    <div className="input-group mb-3">
                                        <input type="text" ref="phone" className={`form-control`} placeholder={here.props.USER.PHONE ? here.props.USER.PHONE : "Please Enter your phone number"} onChange={here.phone_change} />
                                    </div>
                                    <div className="alert_inner" ref="alert_edit" style={{ display: "none" }}>
                                        {here.render_edit_alert()}
                                    </div>
                                </div>
                                {here.render_edit_button()}
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="user_button">
                    <div className="btn-group" role="group" aria-label="Basic example">
                        <button className="btn btn-primary" onClick={() => here.login("fb")} >Facebook</button>
                        <button className="btn btn-primary" onClick={() => here.login("gg")} >Google</button>
                        <button className="btn btn-primary" onClick={here.openTransferModal} >TransferBTC</button>
                    </div>
                    <div className="modal fade" id="transferBTC_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">Transfer Bitcoin</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="input-group mb-3">
                                        <input ref="from_wallet" type="text" className={`form-control${here.state.from_wallet_alert}`} placeholder="Please Enter Your Wallet" maxLength="34" onInput={() => here.changeWallet("from_wallet")} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input ref="to_wallet" type="text" className={`form-control${here.state.to_wallet_alert}`} placeholder="Please Enter Receive Wallet" maxLength="34" onInput={() => here.changeWallet("to_wallet")} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input ref="transferAmount" type="text" className={`form-control`} placeholder="Please Transfer Amount Of Bitcoin" onInput={here.amountChange} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <input ref="transferPK" type="text" className={`form-control${here.state.transferPK_alert}`} placeholder="Please Enter Privatekey Of Your Wallet" maxLength="64" onInput={() => here.changePK("transferPK")} />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={here.transferBTC} >Send</button>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
    componentDidMount() {
        // $('#myModal').on('shown.bs.modal', function () {
        //     $('#myInput').trigger('focus')
        // })
    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(HeaderButton);