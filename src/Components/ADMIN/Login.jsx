import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";

let here;

export class Login extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    changeUsername() {
        here.refs.username.value = here.refs.username.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
    }
    changePassword() {
        here.refs.password.value = here.refs.password.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
    }
    sendLogin() {
        let username = here.refs.username.value;
        let password = here.refs.password.value;
        if (!username || !password) {
            return alert("Missing Information!")
        }
        return here.props.dispatch({
            type: "EMIT",
            connect: "ADMIN_LOGIN_REQUEST",
            data: { username, password }
        })
    }
    render() {
        return (
            <div className="login-panel" >
                <div className="login-title">ADMINISTRATOR LOGIN</div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1"><i className="fas fa-user"></i></span>
                    </div>
                    <input ref="username" type="text" className="form-control" placeholder="Please Enter Username" onInput={here.changeUsername} />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1"><i className="fas fa-key"></i></span>
                    </div>
                    <input ref="password" type="password" className="form-control" placeholder="Please Enter Password" onInput={here.changePassword} />
                </div>
                <div className="login-button">
                    <button type="button" className="btn btn-primary" onClick={here.sendLogin} >Login</button>
                </div>
            </div>
        );
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(Login);