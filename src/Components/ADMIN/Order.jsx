import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";
import { totalmem } from 'os';

let here;
const isValid = wallet => require('bitcore-lib').Address.isValid(wallet, 'livenet');

export class Order extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            searh_target: "ID",
            ORDER: {},
            placeholder: "Enter User ID"
        }
    }
    openDetailModal(e) {
        return here.setState({ ORDER: e }, () => {
            return $("#detail_order_modal").modal("show");
        })
    }
    changeSearhTarget(target) {
        if (here.state.searh_target != target) {
            if (target == "ID") return here.setState({ searh_target: target, placeholder: "Enter User ID" });
            if (target == "KEY") return here.setState({ searh_target: target, placeholder: "Enter Order key" });
            if (target == "TYPE") return here.setState({ searh_target: target, placeholder: "Enter SELL or BUY" });
            if (target == "STATUS") return here.setState({ searh_target: target, placeholder: "Enter -1 or 0 or 1 or 2" });
            if (target == "TOTAL") return here.setState({ searh_target: target, placeholder: "Enter amount of VND" });
            if (target == "AMOUNT") return here.setState({ searh_target: target, placeholder: "Enter amount of Bitcoin" });
            if (target == "ACCOUNT") return here.setState({ searh_target: target, placeholder: "Enter User bank account" });
            if (target == "WALLET") return here.setState({ searh_target: target, placeholder: "Enter wallet" });
            if (target == "CREATETIME") return here.setState({ searh_target: target, placeholder: "Enter number of month" });
            if (target == "ENDTIME") return here.setState({ searh_target: target, placeholder: "Enter number of month" });
        }
    }
    sendSearch() {
        let { target } = here.state;
        let searchInfo = here.refs.searchInfo.value;
        if (target == "ID" && isNaN(tam)) return alert("Please re-check User ID");
        if (target == "KEY" && tam.length != tam.replace(/[ A-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) return alert("Please re-check Order Key");
        if (target == "TYPE" && tam != "BUY" && tam != "SELL") return alert("Please re-check Order Type");
        if (target == "STATUS" && parseInt(tam) < -1 && parseInt(tam) > 2) return alert("Please re-check Order Status");
        if (target == "TOTAL" && (isNaN(tam) || !Number.isInteger(tam))) return alert("Please re-check amount of VND");
        if (target == "AMOUNT" && isNaN(tam)) return alert("Please re-check amount of Bitcoin");
        if (target == "ACCOUNT" && isNaN(tam)) return alert("Please re-check bank account");
        if (target == "WALLET" && !isValid(tam)) return alert("Please re-check wallet");
        if (target == "CREATETIME" && isNaN(tam)) return alert("Please re-check number of month");
        if (target == "ENDTIME" && isNaN(tam)) return alert("Please re-check number of month");
        return here.props.dispatch({
            type: "EMIT",
            connect: "SEARCH_ORDER_REQUEST",
            data: {
                target: here.state.searh_target,
                info: here.refs.searchInfo.value
            }
        });
    }
    formatDay(time) {
        if (time) {
            let now = new Date().getTime();
            let past = new Date(time).getTime();
            let giay = parseInt((now - past) / 1000);
            if (giay < 60) {
                return `${giay} giây trước`;
            } else if (giay < 3600) {
                return `${parseInt(giay / 60)}phút`;
            } else if (giay < 86400) {
                return `${parseInt(giay / 3600)}giờ`;
            } else if (giay < 604800) {
                return `${parseInt(giay / 86400)}ngày`;
            } else if (giay < 2419200) {
                return `${parseInt(giay / 604800)}tuần `;
            }
        } else {
            return "";
        }
    }
    formatToDayTime(x) {
        if (x) {
            let past = new Date(x);
            let time = `${past.getDate()}/${past.getMonth() + 1}/${past.getFullYear()}`;
            return time;
        }
        return "";
    }
    renderDetailModalBody() {
        if (here.state.ORDER.TYPE == "BUY") {
            return (
                <div className="modal-body">
                    <label >ID</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.ID} />
                    </div>
                    <label >KEY</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.KEY} />
                    </div>
                    <label >Wallet</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.WALLET} />
                    </div>
                    <label >Status</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.STATUS} />
                    </div>
                    <label >Create Time</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.formatToDayTime(here.state.ORDER.CREATETIME)} />
                    </div>
                    <label >Finish Time</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.formatToDayTime(here.state.ORDER.ENDTIME)} />
                    </div>
                    <label >Amount Of Bitcoin</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.AMOUNT} />
                    </div>
                    <label >Total Of VND</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.TOTAL} />
                    </div>
                    <label >Status</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.STATUS} />
                    </div>
                </div>
            );
        } else {
            return (
                <div className="modal-body">
                    <label >ID</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.ID} />
                    </div>
                    <label >KEY</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.KEY} />
                    </div>
                    <label >Bank Account</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.ACCOUNT} />
                    </div>
                    <label >Wallet</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.STATUS} />
                    </div>
                    <label >Status</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.STATUS} />
                    </div>
                    <label >Create Time</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.formatToDayTime(here.state.ORDER.CREATETIME)} />
                    </div>
                    <label >Finish Time</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.formatToDayTime(here.state.ORDER.ENDTIME)} />
                    </div>
                    <label >Amount Of Bitcoin</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.AMOUNT} />
                    </div>
                    <label >Total Of VND</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.TOTAL} />
                    </div>
                    <label >Status</label>
                    <div className="input-group mb-3">
                        <input disabled type="text" className="form-control" value={here.state.ORDER.STATUS} />
                    </div>
                </div>
            );
        }
    }
    changesearchIfo(){

    }
    render() {
        return (
            <div id="admin_panel" >
                <div className="panel-title">
                    Quản lý đơn hàng
                        <hr />
                </div>
                <div className="panel-body block">
                    <div className="search-tab">
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{here.state.searh_target}</button>
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("ID")} >ID</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("KEY")} >KEY</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("TYPE")} >TYPE</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("STATUS")}>STATUS</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("TOTAL")}>TOTAL</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("AMOUNT")}>AMOUNT</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("ACCOUNT")}>ACCOUNT</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("WALLET")}>WALLET</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("CREATETIME")}>CREATETIME</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("ENDTIME")}>ENDTIME</a>
                                </div>
                            </div>
                            <input ref="searchInfo" type="text" className="form-control" placeholder={here.state.placeholder} onInput={here.changesearchIfo} />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={here.sendSearch} >Search</button>
                            </div>
                        </div>
                    </div>
                    <div className="user-table">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ID</th>
                                    <th scope="col">KEY</th>
                                    <th scope="col">TYPE</th>
                                    <th scope="col">AMOUNT</th>
                                    <th scope="col">TOTAL</th>
                                    <th scope="col">STATUS</th>
                                    <th scope="col">CREATE TIME</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {here.props.LISTORDER.map((e, i) => {
                                    return (
                                        <tr key={i}>
                                            <td scope="col">{i + 1}</td>
                                            <td scope="col">{e.ID}</td>
                                            <td scope="col">{e.KEY}</td>
                                            <td scope="col">{e.TYPE}</td>
                                            <td scope="col">{e.AMOUNT}</td>
                                            <td scope="col">{e.TOTAL}</td>
                                            <td scope="col">{e.STATUS}</td>
                                            <td scope="col">{here.formatDay(e.CREATETIME)}</td>
                                            <td scope="col">
                                                <div className="btn-group">
                                                    <button type="button" className="btn btn-primary" onClick={() => here.openDetailModal(e)}>
                                                        Detail
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="modal fade" id="detail_order_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Order Detail</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            {here.renderDetailModalBody()}
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(Order);