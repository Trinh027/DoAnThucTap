import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";

let here;

let REGEX = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
let Emailcheck = E => (typeof E === 'string' && E.length > 5 && E.length < 30 && REGEX.test(E)) ? E.toLowerCase() : false;

export class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            searh_target: "ID",
            USER: {},
            edit_click: true,
            email_alert: "",
            nickname_arlert: "",

        }
        this.props.dispatch({
            type: "ADD_USER",
            here
        });
    }
    changeSearhTarget(target) {
        if (here.state.searh_target != target) {
            return here.setState({ searh_target: target });
        }
    }
    sendSearch() {
        return here.props.dispatch({
            type: "EMIT",
            connect: "SEARCH_USER_REQUEST",
            data: {
                target: here.state.searh_target,
                info: here.refs.searchInfo.value
            }
        });
    }
    formatDay(time) {
        if (time) {
            let now = new Date().getTime();
            let past = new Date(time).getTime();
            let giay = parseInt((now - past) / 1000);
            if (giay < 60) {
                return `${giay} giây trước`;
            } else if (giay < 3600) {
                return `${parseInt(giay / 60)} phút trước`;
            } else if (giay < 86400) {
                return `${parseInt(giay / 3600)} giờ trước`;
            } else if (giay < 604800) {
                return `${parseInt(giay / 86400)} ngày trước`;
            } else if (giay < 2419200) {
                return `${parseInt(giay / 604800)} tuần trước`;
            }
        } else {
            return "";
        }
    }
    email_change() {
        if (here.refs.email.value) {
            let tam = here.refs.email.value.trim()
            let a = Emailcheck(tam)
            if (a) {
                here.refs.email.value = tam.toLowerCase();
                here.setState({
                    email_alert: "success"
                })
            } else {
                here.setState({
                    email_alert: "error"
                })
            }
        } else {
            here.setState({
                email_alert: ""
            })
        }
    }
    phone_change() {
        let tam = here.refs.phone.value;
        if (tam.length !== tam.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) {
            here.refs.phone.value = tam.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
        }
    }
    nickname_change() {
        here.refs.nickname.value = here.refs.nickname.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '')
        let tam = here.refs.nickname.value;
        let length = tam.length;
        if (length == 0) {
            return here.setState({ nickname_arlert: "" });
        }
        if ((length < 4 && length > 0) || length > 12) {
            return here.setState({ nickname_arlert: "error" });
        }
        if (length > 3 && length < 13 && tam == here.state.USER.NICKNAME) {
            return here.setState({ nickname_arlert: "error" });
        }
        return here.setState({ nickname_arlert: "success" });
    }
    openDetailModal(e) {
        return here.setState({ USER: e }, () => {
            return $("#detail_modal").modal("show");
        })
    }
    formatToDayTime(x) {
        if (x) {
            let past = new Date(x);
            let time = `${past.getDate()}/${past.getMonth() + 1}/${past.getFullYear()}`;
            return time;
        }
        return "";
    }
    editclick() {
        let edit = 0;
        if (!here.state.edit_click) {
            return alert("You must wait ...")
        }
        return here.setState({ edit_click: false }, () => {
            let tam = {}
            if (here.refs.email.value && here.refs.email.value.length > 0) {
                if (here.state.email_alert === "success") {
                    edit = 1;
                    tam.email = here.refs.email.value
                } else {
                    here.setState({ edit_click: true });
                    return alert("Email error")
                }
            }
            if (here.refs.nickname.value && here.refs.nickname.value.length > 0) {
                if (here.state.nickname_arlert === "success") {
                    edit = 1;
                    tam.nickname = here.refs.nickname.value;
                } else {
                    here.setState({ edit_click: true });
                    return alert("Nickname error");
                }
            }
            if (here.refs.phone.value && here.refs.phone.value.length > 0) {
                edit = 1;
                tam.phone = here.refs.phone.value;
            }
            if (!edit) {
                here.setState({ edit_click: true });
                return alert("You must change something")

            }
            tam.ID = here.state.USER.ID;
            return here.props.dispatch({
                type: "EMIT",
                connect: "EDIT_USER",
                data: tam
            });
        })
    }
    render() {
        return (
            <div id="admin_panel" >
                <div className="panel-title">
                    Quản lý người dùng
                        <hr />
                </div>
                <div className="panel-body block">
                    <div className="search-tab">
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{here.state.searh_target}</button>
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("ID")} >ID</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("EMAIL")} >EMAIL</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("NICKNAME")} >NICKNAME</a>
                                    <a className="dropdown-item" onClick={() => here.changeSearhTarget("PHONE")}>PHONE NUMBER</a>
                                </div>
                            </div>
                            <input ref="searchInfo" type="text" className="form-control" aria-label="Text input with dropdown button" />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={here.sendSearch} >Search</button>
                            </div>
                        </div>
                    </div>
                    <div className="user-table">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Nickname</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Registed</th>
                                    <th scope="col">Last Login</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {here.props.LISTUSER.map((e, i) => {
                                    return (
                                        <tr key={i}>
                                            <td scope="col">{i + 1}</td>
                                            <td scope="col">{e.ID}</td>
                                            <td scope="col">{e.EMAIL}</td>
                                            <td scope="col">{e.NICKNAME}</td>
                                            <td scope="col">{e.PHONE}</td>
                                            <td scope="col">{here.formatDay(e.CREATETIME)}</td>
                                            <td scope="col">{here.formatDay(e.LASTLOGIN)}</td>
                                            <td scope="col">
                                                <div className="btn-group">
                                                    <button type="button" className="btn btn-primary" onClick={() => here.openDetailModal(e)}>
                                                        Detail
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="modal fade" id="detail_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Account Detail</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <label >ID</label>
                                <div className="input-group mb-3">
                                    <input readOnly type="text" className="form-control" value={here.state.USER.ID} />
                                </div>
                                <label >Registed Time</label>
                                <div className="input-group mb-3">
                                    <input readOnly type="text" className="form-control" value={here.formatToDayTime(here.state.USER.CREATETIME)} />
                                </div>
                                <label >Last Login</label>
                                <div className="input-group mb-3">
                                    <input readOnly type="text" className="form-control" value={here.formatToDayTime(here.state.USER.LASTLOGIN)} />
                                </div>
                                <label >Provider</label>
                                <div className="input-group mb-3">
                                    <input readOnly type="text" className="form-control" value={here.state.USER.providerId} />
                                </div>
                                <label>Nickname</label>
                                <div className="input-group mb-3">
                                    <input type="text" ref="nickname" className={`form-control ${here.state.nickname_arlert}`} placeholder={here.state.USER.NICKNAME ? here.state.USER.NICKNAME : "Please enter your nickname (6-12 charactors)"} onChange={here.nickname_change} maxLength="12" />
                                </div>
                                <label>Email</label>
                                <div className="input-group mb-3">
                                    <input type="text" ref="email" className={`form-control ${here.state.email_alert}`} placeholder={here.state.USER.EMAIL ? here.state.USER.EMAIL : "Please enter your email"} onChange={here.email_change} />
                                </div>
                                <label >Phone</label>
                                <div className="input-group mb-3">
                                    <input type="text" ref="phone" className={`form-control`} placeholder={here.state.USER.PHONE ? here.state.USER.PHONE : "Please Enter your phone number"} onChange={here.phone_change} />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary" onClick={here.editclick} >Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(UserInfo);