import React from 'react';

import store from "../../redux.js"
import { connect } from "react-redux";

let here;

export class MassageBox extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    formatDay(time) {
        let now = new Date().getTime();
        let past = new Date(time).getTime();
        let giay = parseInt((now - past) / 1000);
        if (giay < 60) {
            return `${giay} giây trước`;
        } else if (giay < 3600) {
            return `${parseInt(giay / 60)} phút trước`;
        } else if (giay < 86400) {
            return `${parseInt(giay / 3600)} giờ trước`;
        } else if (giay < 604800) {
            return `${parseInt(giay / 86400)} ngày trước`;
        } else if (giay < 2419200) {
            return `${parseInt(giay / 604800)} tuần trước`;
        }
    }
    render() {
        let arr = [];
        for (let i = 0; i < here.props.MASSAGE.MASSAGELIST.length; i++) {
            if (here.props.MASSAGE.MASSAGELIST[i].ID == here.props.READING.ID) {
                arr = arr.concat(here.props.MASSAGE.MASSAGELIST[i].LIST);
                break;
            }
        }
        return arr.map((e, i) => {
            if (isNaN(e.SENDPERSON)) {
                return (
                    <li key={i} className="chat_inner yourself">
                        <div className="content">{e.CONTENT}</div>
                        <div className="info">
                            <span className="nickname">Admin - {here.formatDay(e.TIME)}</span>
                        </div>
                    </li>
                );
            } else {
                return (
                    <li key={i} className="chat_inner">
                        <div className="content">{e.CONTENT}</div>
                        <div className="info">
                            <span className="nickname">{here.props.READING.NICKNAME} - {here.formatDay(e.TIME)}</span>
                        </div>
                    </li>
                );
            }
        })
    }
    componentDidMount() {

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(MassageBox);