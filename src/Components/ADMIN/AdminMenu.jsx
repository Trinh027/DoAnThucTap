import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";

let here;

export class AdminMenu extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    selectMenu(x) {
        return here.props.dispatch({
            type: "SET_SELECT",
            select: x
        })
    }
    render() {
        return (
            <div id="admin_menu">
                <ul className="admin-menu" >
                    <li id="menu_0" className="admin-menu-item" onClick={() => here.selectMenu(0)}>
                        <i className="fas fa-home" ></i>
                        <span className="item-name"  >HOME</span>
                    </li>
                    <li id="menu_1" className="admin-menu-item" onClick={() => here.selectMenu(1)}>
                        <i className="fas fa-cogs" ></i>
                        <span className="item-name" >SYSTEM</span>
                    </li>
                    <li id="menu_2" className="admin-menu-item" onClick={() => here.selectMenu(2)}>
                        <i className="fas fa-users"  ></i>
                        <span className="item-name" >USERS</span>
                    </li>
                    <li id="menu_3" className="admin-menu-item" onClick={() => here.selectMenu(3)}>
                        <i className="fas fa-folder-open"  ></i>
                        <span className="item-name" >ORDER</span>
                    </li>
                    <li id="menu_4" className="admin-menu-item" onClick={() => here.selectMenu(4)}>
                        <i className="fas fa-comment"  ></i>
                        <span className="item-name" >MASSAGE</span>
                    </li>
                    <li id="menu_5" className="admin-menu-item" onClick={() => here.selectMenu(5)}>
                        <i className="fas fa-unlock-alt"  ></i>
                        <span className="item-name" >ADMIN</span>
                    </li>
                </ul>
            </div>
        );
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(AdminMenu);