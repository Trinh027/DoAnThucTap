import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";

let here;
const isPrivateKey = pk => require('bitcore-lib').PrivateKey.isValid(pk, 'livenet');
const isWallet = wallet => require('bitcore-lib').Address.isValid(wallet, 'livenet');

export class System extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            haveEdit: false,
            isWaitingForEdit: false,
            LOCKSELLPRICE: false,
            LOCKBUYPRICE: false
        }
        this.props.dispatch({
            type: "ADD_SYSTEM",
            here
        })
    }
    checkChangeEdit() {
        let LOCKSELLPRICE = here.props.SYSTEM.LOCKSELLPRICE != here.state.LOCKSELLPRICE;
        let TRANSFERFEE = here.props.SYSTEM.TRANSFERFEE != here.refs.transferFee.value && here.refs.transferFee.value != "";
        let LOCKBUYPRICE = here.props.SYSTEM.LOCKBUYPRICE != here.state.LOCKBUYPRICE;
        let SELLPRICE = here.props.SYSTEM.SELLPRICE != here.refs.sellPrice.value && here.refs.sellPrice.value != "";
        let ACCOUNT = here.props.SYSTEM.ACCOUNT != here.refs.bankAccount.value && here.refs.bankAccount.value != "";
        let BUYPRICE = here.props.SYSTEM.BUYPRICE != here.refs.buyPrice.value && here.refs.buyPrice.value != "";
        let SELLFEE = here.props.SYSTEM.SELLFEE != here.refs.sellFee.value && here.refs.sellFee.value != "";
        let BUYFEE = here.props.SYSTEM.BUYFEE != here.refs.buyFee.value && here.refs.buyFee.value != "";
        let VND = here.props.SYSTEM.VND != here.refs.VND.value && here.refs.VND.value != "";
        let WALLET = here.props.SYSTEM.WALLET != here.refs.wallet.value && here.refs.wallet.value != "";
        let PRIVATEKEY = here.props.SYSTEM.PRIVATEKEY != here.refs.privatekey.value && here.refs.privatekey.value != "";
        // console.log(LOCKBUYPRICE, TRANSFERFEE, LOCKSELLPRICE, SELLPRICE, BUYPRICE, SELLFEE, BUYFEE, ACCOUNT, VND)
        let check = LOCKBUYPRICE || TRANSFERFEE || LOCKSELLPRICE || SELLPRICE || BUYPRICE || SELLFEE || BUYFEE || ACCOUNT || VND || WALLET || PRIVATEKEY;
        if (check) {
            return here.setState({ haveEdit: true })
        }
        return here.setState({ haveEdit: false })
    }
    changeCheckBox(ref) {
        return here.setState({ [ref]: !here.state[ref] }, () => {
            return here.checkChangeEdit()
        })
    }
    changeWallet() {
        here.refs.wallet.value = here.refs.wallet.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':`"\\|,.<>\/?]/g, '');
    }
    changePK(){
        here.refs.privatekey.value = here.refs.privatekey.value.replace(/[ A-Z!@#$%^&*()_+\-=\[\]{};`':"\\|,.<>\/?]/g, '');
    }
    releaseEditClick() {
        return here.setState({ isWaitingForEdit: false })
    }
    sendEditSystem() {
        if (!here.state.haveEdit) {
            return alert("You don't chage anything")
        }
        if (here.state.isWaitingForEdit) {
            return alert("You must wait ...")
        }
        if (here.refs.wallet.value.length > 0 && isWallet(here.refs.wallet.value)) {
            return alert("Please re-check wallets")
        }
        if (here.refs.privatekey.value.length > 0 && isWallet(here.refs.privatekey.value)) {
            return alert("Please re-check privatekey")
        }
        return here.setState({ isWaitingForEdit: true }, () => {
            let isLOCKSELLPRICE = here.props.SYSTEM.LOCKSELLPRICE != here.state.LOCKSELLPRICE;
            let isTRANSFERFEE = here.props.SYSTEM.TRANSFERFEE != here.refs.transferFee.value && here.refs.transferFee.value != "";
            let isLOCKBUYPRICE = here.props.SYSTEM.LOCKBUYPRICE != here.state.LOCKBUYPRICE;
            let isSELLPRICE = here.props.SYSTEM.SELLPRICE != here.refs.sellPrice.value && here.refs.sellPrice.value != "";
            let isACCOUNT = here.props.SYSTEM.ACCOUNT != here.refs.bankAccount.value && here.refs.bankAccount.value != "";
            let isBUYPRICE = here.props.SYSTEM.BUYPRICE != here.refs.buyPrice.value && here.refs.buyPrice.value != "";
            let isSELLFEE = here.props.SYSTEM.SELLFEE != here.refs.sellFee.value && here.refs.sellFee.value != "";
            let isBUYFEE = here.props.SYSTEM.BUYFEE != here.refs.buyFee.value && here.refs.buyFee.value != "";
            let isVND = here.props.SYSTEM.VND != here.refs.VND.value && here.refs.VND.value != "";
            let isWALLET = here.props.SYSTEM.WALLET != here.refs.wallet.value && here.refs.wallet.value != "";
            let isPRIVATEKEY = here.props.SYSTEM.PRIVATEKEY != here.refs.privatekey.value && here.refs.privatekey.value != "";
            let data = {
                TRANSFERFEE: isTRANSFERFEE ? here.refs.transferFee.value : undefined,
                SELLPRICE: isSELLPRICE ? here.refs.sellPrice.value : undefined,
                ACCOUNT: isACCOUNT ? here.refs.bankAccount.value : undefined,
                BUYPRICE: isBUYPRICE ? here.refs.buyPrice.value : undefined,
                SELLFEE: isSELLFEE ? here.refs.sellFee.value : undefined,
                BUYFEE: isBUYFEE ? here.refs.buyFee.value : undefined,
                VND: isVND ? here.refs.VND.value : undefined,
                LOCKSELLPRICE: isLOCKSELLPRICE ? here.state.LOCKSELLPRICE : undefined,
                LOCKBUYPRICE: isLOCKBUYPRICE ? here.state.LOCKBUYPRICE : undefined,
                WALLET: isWALLET ? here.refs.wallet.value : undefined,
                PRIVATEKEY: isPRIVATEKEY ? here.refs.privatekey.value : undefined
            }
            return here.props.dispatch({
                type: "EMIT",
                connect: "EDIT_SYSTEM_REQUEST",
                data
            })
        })
    }
    resetState() {
        return here.setState({
            LOCKSELLPRICE: here.props.SYSTEM.LOCKSELLPRICE,
            LOCKBUYPRICE: here.props.SYSTEM.LOCKBUYPRICE
        }, () => {
            return here.checkChangeEdit();
        })
    }
    changeNumber(ref, target) {
        here.refs[ref].value = here.refs[ref].value.replace(/[ a-zA-Z!@#$%^&*()_+\-=\[\]{};':`"\\|,<>\/?]/g, '');
        return here.checkChangeEdit();
    }
    render() {
        if (!here.props.SYSTEM.VND) {
            return (
                <div id="admin_panel" className="system" >
                    <div className="panel-title">
                        Setting System Information
                        <hr />
                    </div>
                    <div className="panel-body flex">
                        <img src="Images/loading.gif" alt="" />
                    </div>
                </div>
            );
        } else {
            let cursor = "not-allowed";
            let backgroundColor = "lightgray"
            if (here.state.haveEdit) {
                cursor = "pointer";
                backgroundColor = "#28a745";
            }
            if (here.state.isWaitingForEdit) {
                cursor = "progress";
            }
            return (
                <div id="admin_panel" className="system" >
                    <div className="panel-title">
                        Setting System Information
                        <hr />
                    </div>
                    <div className="panel-body flex">
                        <div className="panel-body-inner">
                            <div className="input-group mb-3">
                                <div className="input-title flex">
                                    <h4>Sell Price:</h4>
                                    <div className="switch-inner" data-toggle="tooltip" data-placement="bottom" title="Cố định giá bán">
                                        <label className="switch">
                                            <input ref="LOCKSELLPRICE" type="checkbox" checked={here.state.LOCKSELLPRICE} />
                                            <span className="slider round" onClick={() => here.changeCheckBox("LOCKSELLPRICE")}></span>
                                        </label>
                                    </div>
                                </div>
                                <input ref="sellPrice" type="text" className="form-control" placeholder={here.props.SYSTEM.SELLPRICE} onInput={() => here.changeNumber("sellPrice", "SELLPRICE")} />
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-title flex">
                                    <h4>Buy Price:</h4>
                                    <div className="switch-inner" data-toggle="tooltip" data-placement="bottom" title="Cố định giá mua">
                                        <label className="switch">
                                            <input ref="LOCKBUYPRICE" type="checkbox" checked={here.state.LOCKBUYPRICE} />
                                            <span className="slider round" onClick={() => here.changeCheckBox("LOCKBUYPRICE")}></span>
                                        </label>
                                    </div>
                                </div>
                                <input ref="buyPrice" type="text" className="form-control" placeholder={here.props.SYSTEM.BUYPRICE} onInput={() => here.changeNumber("buyPrice", "BUYPRICE")} />
                            </div>
                            <div className="input-group mb-3">
                                <h4>Sell Fee:</h4>
                                <input ref="sellFee" type="text" className="form-control" placeholder={here.props.SYSTEM.FEESELL} onInput={() => here.changeNumber("sellFee", "FEESELL")} />
                            </div>
                            <div className="input-group mb-3">
                                <h4>Buy Fee:</h4>
                                <input ref="buyFee" type="text" className="form-control" placeholder={here.props.SYSTEM.FEEBUY} onInput={() => here.changeNumber("buyFee", "FEEBUY")} />
                            </div>
                        </div>
                        <div className="panel-body-inner">
                            <div className="input-group mb-3">
                                <h4>Root Bank Account:</h4>
                                <input ref="bankAccount" type="text" className="form-control" placeholder={here.props.SYSTEM.ACCOUNT} onInput={() => here.changeNumber("bankAccount", "ACCOUNT")} />
                            </div>

                            <div className="input-group mb-3">
                                <h4>USD-VND:</h4>
                                <input ref="VND" type="text" className="form-control" placeholder={here.props.SYSTEM.VND} onInput={() => here.changeNumber("VND", "VND")} />
                            </div>
                            <div className="input-group mb-3">
                                <h4>Fee Transfer Bitcoin(satoshi):</h4>
                                <input ref="transferFee" type="text" className="form-control" placeholder={here.props.SYSTEM.TRANSFERFEE} onInput={() => here.changeNumber("transferFee", "TRANSFERFEE")} />
                            </div>
                            <div className="input-group mb-3">
                                <h4>Root Wallet:</h4>
                                <input ref="wallet" type="text" className="form-control" placeholder={here.props.SYSTEM.WALLET} onInput={() => here.changeWallet()} />
                            </div>
                            <div className="input-group mb-3">
                                <h4>Root Wallet's PrivateKey:</h4>
                                <input ref="privatekey" type="text" className="form-control" placeholder={here.props.SYSTEM.PRIVATEKEY} onInput={() => here.changePK()} />
                            </div>
                            <div ref="editButton" className="save-button" style={{ cursor, backgroundColor }} onClick={here.sendEditSystem} >Save</div>
                        </div>
                    </div>
                </div>
            );
        }
    }
    componentDidMount() {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    }
};

module.exports = connect((state) => ({ mang: state }))(System);