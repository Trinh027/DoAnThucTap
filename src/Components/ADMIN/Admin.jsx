import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";

let here;

export class Home extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            password_alert: "",
            repassword_alert: "",
            username_alert: ""
        }
    }
    openCreateModal(e) {
        return $("#detail_admin_modal").modal("show");
    }
    sendCreateadmin() {
        if (here.state.username_alert != " success") {
            return alert("Please Re-check Username");
        }
        if (here.state.password_alert != " success") {
            return alert("Please Re-check Password");
        }
        if (here.state.repassword_alert != " success") {
            return alert("Please Re-check Re-enter Password");
        }
        return here.props.dispatch({
            type: "EMIT",
            connect: "CREATE_ADMIN_REQUEST",
            data: {
                ID: here.props.USER.USERNAME,
                username: here.refs.username.value,
                password: here.refs.password.value
            }
        })
    }
    changeSearhTarget(target) {
        if (here.state.searh_target != target) {
            if (target == "ID") return here.setState({ searh_target: target, placeholder: "Enter Username" });
            if (target == "STATUS") return here.setState({ searh_target: target, placeholder: "Enter 0 or 1" });
            if (target == "CREATETIME") return here.setState({ searh_target: target, placeholder: "Enter number of month" });
            if (target == "ENDTIME") return here.setState({ searh_target: target, placeholder: "Enter number of month" });
        }
    }
    changeUsername() {
        here.refs.username.value = here.refs.username.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
        let tam = here.refs.username.value;
        if (tam.length == 0) {
            return here.setState({ username_alert: "" })
        }
        if (tam.length < 6 || tam.length > 12) {
            return here.setState({ username_alert: " error" })
        }
        if (!isNaN(tam)) {
            return here.setState({ username_alert: " error" })
        }
        return here.setState({ username_alert: " success" })
    }
    password_change() {
        here.refs.password.value = here.refs.password.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
        here.refs.repassword.value = here.refs.repassword.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
        let pass = here.refs.password.value;
        let repass = here.refs.repassword.value;
        if (pass.length > 0) {
            if (pass.length < 6 || pass.length > 8) {
                here.setState({ password_alert: " error" })
            } else {
                here.setState({ password_alert: " success" })
            }
        } else {
            here.setState({ password_alert: "" })
        }
        here.refs.repassword.value = null;
        here.setState({ repassword_alert: "" })
    }
    repassword_change() {
        here.refs.password.value = here.refs.password.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
        here.refs.repassword.value = here.refs.repassword.value.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '');
        let pass = here.refs.password.value;
        let repass = here.refs.repassword.value;
        if (repass.length > 0) {
            if (here.state.password_alert == " success") {
                if (pass == repass) {
                    here.setState({ repassword_alert: " success" })
                } else {
                    here.setState({ repassword_alert: " error" })
                }
            } else {
                here.refs.repassword.value = null;
                here.setState({ repassword_alert: "" })
            }
        } else {
            here.setState({ repassword_alert: "" })
        }
    }
    formatDay(time) {
        if (time) {
            let now = new Date().getTime();
            let past = new Date(time).getTime();
            let giay = parseInt((now - past) / 1000);
            if (giay < 60) {
                return `${giay} giây trước`;
            } else if (giay < 3600) {
                return `${parseInt(giay / 60)} phút`;
            } else if (giay < 86400) {
                return `${parseInt(giay / 3600)} giờ`;
            } else if (giay < 604800) {
                return `${parseInt(giay / 86400)} ngày`;
            } else if (giay < 2419200) {
                return `${parseInt(giay / 604800)} tuần `;
            }
        } else {
            return "";
        }
    }
    formatToDayTime(x) {
        if (x) {
            let past = new Date(x);
            let time = `${past.getDate()}/${past.getMonth() + 1}/${past.getFullYear()}`;
            return time;
        }
        return "";
    }
    render() {
        if (here.props.LISTADMIN.length == 0) {
            return (
                <div id="admin_panel" >
                    <div className="panel-title">
                        ADMINISTRATORS
                        <hr />
                    </div>
                    <div className="panel-body flex">
                        <img src="Images/loading.gif" alt="" />
                    </div>
                </div>
            );
        } else {
            return (
                <div id="admin_panel" >
                    <div className="panel-title">
                        ADMINISTRATORS
                            <hr />
                    </div>
                    <div className="panel-body block">
                        <div className="search-tab">
                            <button type="button" className="btn btn-primary" onClick={here.openCreateModal} >+ New Admin</button>
                        </div>
                        <div className="user-table">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">USERNAME</th>
                                        <th scope="col">CREATE TIME</th>
                                        <th scope="col">LAST LOGIN</th>
                                        <th scope="col">CREATER</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {here.props.LISTADMIN.map((e, i) => {
                                        return (
                                            <tr key={i}>
                                                <td scope="col">{i + 1}</td>
                                                <td scope="col">{e.USERNAME}</td>
                                                <td scope="col">{here.formatDay(e.CREATETIME)}</td>
                                                <td scope="col">{here.formatDay(e.LASTLOGIN)}</td>
                                                <td scope="col">{e.CREATER}</td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="modal fade" id="detail_admin_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">New Admin</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="basic-addon1"><i className="fas fa-user"></i></span>
                                        </div>
                                        <input ref="username" type="text" className={`form-control${here.state.username_alert}`} placeholder="Please Enter Username" maxLength="12" onInput={here.changeUsername} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="basic-addon1"><i className="fas fa-key"></i></span>
                                        </div>
                                        <input ref="password" type="password" className={`form-control${here.state.password_alert}`} placeholder="Please Enter Password" maxLength="8" onInput={here.password_change} />
                                    </div>
                                    <div className="input-group mb-3">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="basic-addon1"><i className="fas fa-key"></i></span>
                                        </div>
                                        <input ref="repassword" type="password" className={`form-control${here.state.repassword_alert}`} placeholder="Please Re-enter Password" maxLength="8" onInput={here.repassword_change} />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" onClick={here.sendCreateadmin} >Create</button>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(Home);