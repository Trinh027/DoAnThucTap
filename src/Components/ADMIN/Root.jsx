import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";
import ck from "js-cookie";
import io from "socket.io-client";

import Login from "./Login.jsx";
import AdminMenu from "./AdminMenu.jsx";
import AdminPanel from "./AdminPanel.jsx";
import Chat from "./Chat.jsx";

let here;

export class Root extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            SOCKET: io(),
            USER: { USERNAME: 0 },
            SYSTEM: {},
            CHAT: {
                PUBLIC: []
            },
            LISTUSER: [],
            LISTORDER: [],
            MASSAGE: {
                MASSAGELIST: [],
                UNREADLIST: []
            },
            REPORT: {},
            LISTADMIN: [],
            SELECT: 0
        }
        this.props.dispatch({
            type: "ADD_ROOT",
            here
        })
    }
    socketEmit(connect, data, cb) {
        here.state.SOCKET.emit(connect, JSON.stringify(data));
        if (cb) return cb;
        return 1;
    }
    requestPanelData(x) {
        if (x == 0) {
            return here.setState({ REPORT: {} }, () => {
                return here.socketEmit("REPORT_REQUEST", { USERNAME: here.state.USER.USERNAME })
            })
        }
        if (x == 1) {
            return here.setState({ SYSTEM: {} }, () => {
                return here.socketEmit("REQUEST_SYSTEM", { USERNAME: here.state.USER.USERNAME })
            })
        }
        if (x == 5) {
            return here.setState({ LISTADMIN: [] }, () => {
                return here.socketEmit("REQUEST_LISTADMIN", { USERNAME: here.state.USER.USERNAME })
            })
        }
    }
    setSelect(x) {
        if (here.state.SELECT != x) {
            $(`#menu_${here.state.SELECT}`).removeClass("active");
            return here.setState({ SELECT: x }, () => {
                $(`#menu_${here.state.SELECT}`).addClass("active");
                return here.requestPanelData(here.state.SELECT);
            });
        }
    }
    removeMassageList(ID) {
        let temp = Object.assign({}, here.state.MASSAGE);
        for (let i = 0; i < temp.MASSAGELIST.length; i++) {
            if (temp.MASSAGELIST[i].ID == ID) {
                temp.MASSAGELIST.splice(i, 1);
            }
        }
        return here.setState({ MASSAGE: temp });
    }
    removeUnReadList(ID) {
        let temp = Object.assign({}, here.state.MASSAGE);
        let index = temp.UNREADLIST.indexOf(ID);
        if (index > -1) {
            temp.UNREADLIST.splice(index, 1);
        }
        return here.setState({ MASSAGE: temp });
    }
    render() {
        if (!here.state.USER.USERNAME) {
            return (
                <div className="container-fluid" >
                    <Login />
                </div>
            );
        } else {
            return (
                <div className="container-fluid" >
                    <div className="row">
                        <AdminMenu
                            USER={here.state.USER}
                            SELECT={here.state.SELECT}
                        />
                        <AdminPanel
                            USER={here.state.USER}
                            SELECT={here.state.SELECT}
                            SYSTEM={here.state.SYSTEM}
                            LISTUSER={here.state.LISTUSER}
                            LISTORDER={here.state.LISTORDER}
                            LISTADMIN={here.state.LISTADMIN}
                            MASSAGE={here.state.MASSAGE}
                            REPORT={here.state.REPORT}
                        />
                        <Chat
                            show={parseInt(ck.get("CHATBOX"))}
                            USER={here.state.USER}
                            CHAT={here.state.CHAT}
                        />
                    </div>
                </div>
            );
        }
    }
    componentDidMount() {
        here.setSelect(4)
        here.state.SOCKET.on("ADMIN_LOGIN_RESPONSE", data => {
            data = JSON.parse(data);
            if (data.STATUS == 0) {
                return alert(data.NOTE);
            }
            return here.setState({ USER: data.USER }, () => {
                here.props.dispatch({
                    type: "MOVE_CHAT_TO_BOTTOM"
                })
                return here.socketEmit("MASSAGE_REQUEST", { USERNAME: here.state.USER.USERNAME });
            });
        });
        here.state.SOCKET.on("SYSTEM_RESPONSE", data => {
            data = JSON.parse(data);
            return here.setState({ SYSTEM: data }, () => {
                here.props.dispatch({
                    type: "RESET_SYSTEM"
                })
                here.state.SOCKET.on("UPDATE_PRICE", data => {
                    data = JSON.parse(data);
                    let tam = Object.assign({}, here.state.SYSTEM);
                    tam[data.TYPE] = parseInt(data.VALUE);
                    here.setState({ SYSTEM: tam });
                })
            });
        });
        here.state.SOCKET.on("EDIT_RESPONSE", data => {
            data = JSON.parse(data);
            if (data.STATUS == 0) {
                return alert(data.NOTE);
            }
            here.requestPanelData(here.state.SELECT);
            return here.props.dispatch({
                type: "RELEASE_EDIT_CLICK"
            })
        });
        here.socketEmit("PUBLIC_CHAT_REQUEST");
        here.state.SOCKET.on("PUBLIC_CHAT_RESPONSE", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.CHAT);
            tam.PUBLIC = data;
            here.setState({ CHAT: tam }, () => {
                return here.props.dispatch({
                    type: "MOVE_CHAT_TO_BOTTOM"
                })
            })
        });
        here.state.SOCKET.on("SEND_PUBLICCHAT", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.CHAT);
            tam.PUBLIC.push(data);
            here.setState({ CHAT: tam }, () => {
                return here.props.dispatch({
                    type: "MOVE_CHAT_TO_BOTTOM"
                })
            })
        });
        here.state.SOCKET.on("SEARCH_USER_RESPONSE", data => {
            data = JSON.parse(data);
            return here.setState({ LISTUSER: data })
        });
        here.state.SOCKET.on("MASSAGE_RESPONSE", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.MASSAGE)
            tam.MASSAGELIST = data
            return here.setState({ MASSAGE: tam })
        });
        here.state.SOCKET.on("REPORT_RESPONSE", data => {
            data = JSON.parse(data);
            return here.setState({ REPORT: data })
        });
        here.state.SOCKET.on("SEARCH_ORDER_RESPONE", data => {
            data = JSON.parse(data);
            return here.setState({ LISTORDER: data })
        });
        here.state.SOCKET.on("DELETE_PRIVATECHAT", data => {
            data = JSON.parse(data);
            return here.removeMassageList(data.ID)
        });
        here.state.SOCKET.on("SEND_PRIVATECHAT", data => {
            data = JSON.parse(data);
            let tam = Object.assign({}, here.state.MASSAGE);
            let flat = true;
            for (let i = 0; i < tam.MASSAGELIST.length; i++) {
                if (tam.MASSAGELIST[i].ID == data.ID) {
                    if (tam.MASSAGELIST[i].NICKNAME != data.NICKNAME) tam.MASSAGELIST[i].NICKNAME = data.NICKNAME;
                    tam.MASSAGELIST[i].LIST.push(data);
                    flat = false;
                    break;
                }
            }
            if (flat) {
                tam.MASSAGELIST.push({ ID: data.ID, NICKNAME: data.NICKNAME, LIST: [data] })
            }
            tam.UNREADLIST.push(data.ID)
            return here.setState({ MASSAGE: tam }, () => {
                return here.props.dispatch({
                    type: "MOVE_MASSAGE_TO_BOTTOM"
                })
            })
        });
        here.state.SOCKET.on("CREATE_ADMIN_RESPONSE", data => {
            data = JSON.parse(data);
            if (data.STATUS == 1) {
                let tam = [data.ADMIN].concat(here.state.LISTADMIN);
                return here.setState({ LISTADMIN: tam }, () => alert(data.NOTE));
            }
            return alert(data.NOTE);
        });
        here.state.SOCKET.on("RESPONSE_LISTADMIN", data => {
            data = JSON.parse(data);
            return here.setState({ LISTADMIN: data });

        });
    }
};

module.exports = connect((state) => ({ mang: state }))(Root);