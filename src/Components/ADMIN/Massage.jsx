import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux"

import MassageBox from "./MassageBox.jsx";

let here;

export class Massage extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            reading: {
                ID: "",
                NICKNAME: ""
            }
        }
        this.props.dispatch({
            type: "ADD_MASSAGE",
            here
        })
    }
    openMassageModal(e) {
        return here.setState({ reading: { ID: e.ID, NICKNAME: e.NICKNAME } }, () => {
            here.props.dispatch({
                type: "REMOVE_UNREADLIST",
                ID: e.ID
            })
            return $("#massage_modal").modal("show");
        })
    }
    moveToBottom() {
        let massageBox = document.getElementById("massageBox")
        if (massageBox) return $("#massageBox").scrollTop(massageBox.offsetHeight * 2);
    }
    sendMessenge() {
        if (!here.props.USER.USERNAME) {
            return alert("Please Login!");
        }
        if (!here.refs.chatInput.value && here.refs.chatInput.value.length == 0) {
            return alert("Please Enter Something!");
        }
        let obj = {
            ID: here.state.reading.ID,
            CONTENT: here.refs.chatInput.value.trim(),
            NICKNAME: here.state.reading.NICKNAME
        }
        if (obj.CONTENT == "/deletethis") {
            here.props.dispatch({
                type: "REMOVE_MASSAGELIST",
                ID: obj.ID
            })
            $("#massage_modal").modal("hide");
        }
        here.props.dispatch({
            type: "EMIT",
            connect: "ADMIN_PRIVATE_CHAT_SEND",
            data: obj
        })
        here.refs.chatInput.value = null;
    }
    writeMassage(event) {
        if (event.key == 'Enter') {
            return here.sendMessenge()
        }
    }
    render() {
        if (here.props.MASSAGE.MASSAGELIST.length == 0) {
            return (
                <div id="admin_panel" className="system" >
                    <div className="panel-title">
                        Massage
                        <hr />
                    </div>
                    <div className="panel-body flex">
                        <img src="Images/loading.gif" alt="" />
                    </div>
                </div>
            );
        } else {
            return (
                <div id="admin_panel" >
                    <div className="panel-title">
                        Massage
                        <hr />
                    </div>
                    <div className="panel-body">
                        <div className="massage-inner flex">
                            {here.props.MASSAGE.MASSAGELIST.map((e, i) => {
                                let addClass = (here.props.MASSAGE.UNREADLIST.indexOf(e.ID) != -1 && e.ID != here.state.reading.ID) ? " red" : " ";
                                return (
                                    <div key={i} className="massage-tab" onClick={() => here.openMassageModal(e)} >
                                        <div className={`massage-tab-nickname${addClass}`}>{e.NICKNAME}</div>
                                        <div className="massage-tab-massage">{e.LIST[e.LIST.length - 1].CONTENT}</div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div className="modal fade" id="massage_modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">Private Chat With {here.state.reading.NICKNAME}</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <ul className="chatbox" id="massageBox" >
                                        <MassageBox
                                            USER={here.props.USER}
                                            MASSAGE={here.props.MASSAGE}
                                            READING={here.state.reading}
                                        />
                                    </ul>
                                </div>
                                <div className="inputzone">
                                    <div className="input-group mb-3">
                                        <input ref="chatInput" type="text" className="form-control" placeholder="..." onKeyPress={here.writeMassage} maxLength="200" />
                                        <div className="input-group-append">
                                            <button className="btn btn-primary" type="button" onClick={here.sendMessenge} >Send</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(Massage);