import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";

let here;

export class Home extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    formatNum(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        if (!here.props.REPORT.totalOrder) {
            return (
                <div id="admin_panel" className="system" >
                    <div className="panel-title">
                        Báo cáo tổng quang
                        <hr />
                    </div>
                    <div className="panel-body flex">
                        <img src="Images/loading.gif" alt="" />
                    </div>
                </div>
            );
        } else {
            let completeOrder = parseInt(here.props.REPORT.totalCompleteOrder / here.props.REPORT.totalOrder * 10000) / 100;
            let cancelOrer = parseInt(here.props.REPORT.totalCancelOrder / here.props.REPORT.totalOrder * 10000) / 100;
            let inProcessOrder = parseInt(here.props.REPORT.totalInProcessOrder / here.props.REPORT.totalOrder * 10000) / 100;
            let waitingOrder = parseInt(here.props.REPORT.totalWaitingOrder / here.props.REPORT.totalOrder * 10000) / 100;
            let persenBTC = parseInt(here.props.REPORT.totalOutBTC / here.props.REPORT.totalInBTC * 10000) / 100;
            let persenVND = parseInt(here.props.REPORT.totalOutVND / here.props.REPORT.totalInVND * 10000) / 100;
            return (
                <div id="admin_panel" >
                    <div className="panel-title">
                        Báo cáo tổng quang
                            <hr />
                    </div>
                    <div className="panel-body block home">
                        <div className="home-line">
                            <div className="home-line-title">Tổng số giao dịch được tạo</div>
                            <div className="home-line-info">{here.props.REPORT.totalOrder}</div>

                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số giao dịch hoàn thành</div>
                            <div className="home-line-info">{here.props.REPORT.totalCompleteOrder}</div>
                            <div className="home-line-persen">{completeOrder}%</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số giao dịch bị hủy</div>
                            <div className="home-line-info">{here.props.REPORT.totalCancelOrder}</div>
                            <div className="home-line-persen">{cancelOrer}%</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số giao dịch đang xữ lý</div>
                            <div className="home-line-info">{here.props.REPORT.totalInProcessOrder}</div>
                            <div className="home-line-persen">{inProcessOrder}%</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số giao dịch đang chờ</div>
                            <div className="home-line-info">{here.props.REPORT.totalWaitingOrder}</div>
                            <div className="home-line-persen">{waitingOrder}%</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số Bitcoin đã nhận</div>
                            <div className="home-line-info">{here.props.REPORT.totalInBTC} BTC</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số Bitcoin đã chuyển ra</div>
                            <div className="home-line-info">{here.props.REPORT.totalOutBTC} BTC</div>
                            <div className="home-line-persen">{persenBTC}%</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số VND đã nhận</div>
                            <div className="home-line-info">{here.formatNum(here.props.REPORT.totalInVND)} VND</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Tổng số VND đã chuyển ra</div>
                            <div className="home-line-info">{here.formatNum(here.props.REPORT.totalOutVND)} VND</div>
                            <div className="home-line-persen">{persenVND}%</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch BUY cao nhất(Bitcoin):</div>
                            <div className="home-line-info">{here.props.REPORT.maxBuyBTC.value} BTC</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.maxBuyBTC.key}</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch BUY thấp nhất(Bitcoin):</div>
                            <div className="home-line-info">{here.props.REPORT.minBuyBTC.value} BTC</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.minBuyBTC.key}</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch SELL cao nhất(Bitcoin):</div>
                            <div className="home-line-info">{here.props.REPORT.maxSellBTC.value} BTC</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.maxSellBTC.key}</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch SELL thấp nhất(Bitcoin):</div>
                            <div className="home-line-info">{here.props.REPORT.minSellBTC.value} BTC</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.minSellBTC.key}</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch BUY cao nhất(VND)</div>
                            <div className="home-line-info">{here.formatNum(here.props.REPORT.maxBuyVND.value)} VND</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.maxBuyVND.key}</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch BUY thấp nhất(VND)</div>
                            <div className="home-line-info">{here.formatNum(here.props.REPORT.minBuyVND.value)} VND</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.minBuyVND.key}</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch SELL cao nhất(VND)</div>
                            <div className="home-line-info">{here.formatNum(here.props.REPORT.maxSellVND.value)} VND</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.maxSellVND.key}</div>
                        </div>
                        <hr />
                        <div className="home-line">
                            <div className="home-line-title">Giao dịch SELL thấp nhất(VND)</div>
                            <div className="home-line-info">{here.formatNum(here.props.REPORT.minSellVND.value)} VND</div>
                            <div className="home-line-persen">KEY: {here.props.REPORT.minSellVND.key}</div>
                        </div>
                        <hr />
                    </div>
                </div>
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(Home);