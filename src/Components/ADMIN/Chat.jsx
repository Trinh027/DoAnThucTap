import React from 'react';
import ck from "js-cookie";

import store from "../../redux.js"
import { connect } from "react-redux";

import ChatBox from "./ChatBox.jsx";

let here;

export class Chat extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {
            show: here.props.show
        }
        this.props.dispatch({
            type: "ADD_CHAT",
            here
        })
    }
    changeShow() {
        here.setState({ show: here.state.show ? 0 : 1 }, () => {
            ck.set("CHATBOX", here.state.show);
            here.moveToBottom(true);
        });
    }
    moveToBottom() {
        let chatBox = document.getElementById("chatBox")
        if (chatBox) return $("#chatBox").scrollTop(chatBox.offsetHeight * 2);
    }
    sendMessenge() {
        if (!here.props.USER.USERNAME) {
            return alert("Please Login!");
        }
        if (!here.refs.chatInput.value && here.refs.chatInput.value.length == 0) {
            return alert("Please Enter Something!");
        }
        let obj = {
            ID: here.props.USER.USERNAME,
            CONTENT: here.refs.chatInput.value,
            NICKNAME: here.props.USER.USERNAME
        }
        here.props.dispatch({
            type: "EMIT",
            connect: "PUBLIC_CHAT_SEND",
            data: obj
        })
        here.refs.chatInput.value = null;
    }
    writeMassage(event) {
        if (event.key == 'Enter') {
            return here.sendMessenge()
        }
    }
    render() {
        if (here.state.show) {
            return (
                <div className="chat">
                    <div className="chat_setting" onClick={here.changeShow} >
                        Hide ChatBox
                    </div>
                    <ul className="chatbox" id="chatBox">
                        <ChatBox
                            public={true}
                            USER={here.props.USER}
                            CHAT={here.props.CHAT}
                        />
                    </ul>
                    <div className="inputzone">
                        <div className="input-group mb-3">
                            <input ref="chatInput" type="text" className="form-control" placeholder="..." onKeyPress={here.writeMassage} maxLength="200" />
                            <div className="input-group-append">
                                <button className="btn btn-primary" type="button" onClick={here.sendMessenge} >Send</button>
                            </div>
                        </div>
                    </div>
                </div >
            );
        } else {
            return (
                <div className="chat hide" onClick={here.changeShow}>
                    <div className="chat_setting">
                        <i className="far fa-comment-alt"></i>
                    </div>
                </div >
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect(function (state) {
    return { mang: state }
})(Chat);