import React from 'react';

import store from "../../ADMIN-redux.js"
import { connect } from "react-redux";

import Home from "./Home.jsx";
import Massage from "./Massage.jsx";
import Order from "./Order.jsx";
import System from "./System.jsx";
import UserInfo from "./UserInfo.jsx";
import Admin from "./Admin.jsx";

let here;

export class AdminPanel extends React.Component {
    constructor(props) {
        super(props);
        here = this;
        this.state = {

        }
    }
    render() {
        if (here.props.SELECT == 0) {
            return (
                <Home
                    REPORT={here.props.REPORT}
                />
            );
        } else if (here.props.SELECT == 1) {
            return (
                <System
                    USER={here.props.USER}
                    SYSTEM={here.props.SYSTEM}
                />
            );
        } else if (here.props.SELECT == 2) {
            return (
                <UserInfo
                    USER={here.props.USER}
                    LISTUSER={here.props.LISTUSER}
                />
            );
        } else if (here.props.SELECT == 3) {
            return (
                <Order
                    USER={here.props.USER}
                    LISTORDER={here.props.LISTORDER}
                />
            );
        } else if (here.props.SELECT == 4) {
            return (
                <Massage
                    USER={here.props.USER}
                    MASSAGE={here.props.MASSAGE}
                />
            );
        } else if (here.props.SELECT == 5) {
            return (
                <Admin
                    USER={here.props.USER}
                    LISTADMIN={here.props.LISTADMIN}
                />
            );
        }
    }
    componentDidMount() {

    }
};

module.exports = connect((state) => ({ mang: state }))(AdminPanel);