const express = require('express');

const app = express();

const server = require('http').Server(app);
const io = require('socket.io')(server);

const cookieParser = require('cookie-parser');

const cors = require('cors');
const { json } = require('body-parser');

app.set('view engine', 'ejs');
app.set('views', './views');
app.use(express.static('public'));
app.use(cookieParser());
app.use(json());
app.use(cors());

app.use('/', require('./router/router_controller.js'));

io.on('connection', socket => require('./socket/socket_controller.js')(io, socket));

require("./controller/System.js").loadSystem((b, s) => {
    console.log("Load System!");
    server.listen(7000, () => console.log("Start!"));
    require("./controller/GetPrice.js").startListen(b, s, (type, price) => {
        return io.emit("UPDATE_PRICE", JSON.stringify({ TYPE: type, VALUE: price }));
    });
    require("./controller/Withdrawal.js").loadWithdrawal(() => {
        console.log("Load Withdrawal!");
    })
})


require("./controller/Order.js").listenOrder();




