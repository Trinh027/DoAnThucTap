const path = require('path');
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const srcPath = path.resolve(__dirname, 'src');
const dstPath = path.resolve(__dirname, 'dst');

const sassLoaders = [
    'css-loader?minimize',
    'sass-loader?indentedSyntax=sass&includePaths[]=' + srcPath
];

module.exports = {
    mode: 'development',
    entry: {
        UI: './src/UI-app.jsx',
        ADMIN: './src/ADMIN-app.jsx'
    },
    optimization: {
        minimize: false
    },
    output: {
        filename: '[name]-bundle.js',
        path: __dirname + '/public'
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        // new webpack.LoaderOptionsPlugin({
        //     minimize: true
        // }),
        // new UglifyJsPlugin({
        //     sourceMap: true
        // })
    ],
    performance: {
        hints: process.env.NODE_ENV === 'production' ? "warning" : false
    },
    module: {
        rules: [
            {
                loader: 'babel-loader',
                test: /\.jsx?$/,
                query: { presets: ['react', 'es2015', 'env', 'stage-0'] },
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    }
};