let { updateValue } = require("./System.js");

let io = require("socket.io-client");
// let socket = io.connect('https://coincap.io');
// socket.on('trades', res => {
//     // if (res.exchange_id == "bitfinex") {
//     //     console.log(res.coin)
//     // }
//     if (res.coin == "BTC") {
//         // res.msg.price
//         console.log(res.exchange_id);
//     }
// })

let buy = 0;
let sell = 0;
let flat = true;

let socket = io.connect("wss://streamer.cryptocompare.com");

function startListen(b, s, cb) {
    buy = b;
    sell = s;
    socket.emit('SubAdd', { subs: ['0~Bitstamp~BTC~USD'] });
    socket.on('m', function (currentData) {
        if (flat) {
            flat = false;
            let mang = currentData.split("~");
            if ((mang[4] == 1 && mang[8] != sell) || (mang[4] == 2 && mang[8] != buy) && !isNaN(mang[8])) {
                mang[4] == 1 ? sell = mang[8] : buy = mang[8];
                updateValue(mang[4], mang[8], cb)
            }
            return flat = true;
        }
    });
}

module.exports = { startListen };
