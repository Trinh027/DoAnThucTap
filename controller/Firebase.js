let admin = require("firebase-admin");
let { getAccount } = require("./System.js");
let { newBTCWallet } = require("./BTC.js");
let { getSocketByID } = require("./User.js");
let { addLogToList } = require("./LogWriter.js")

const serviceAccount = require("./muaban-coin-firebase-adminsdk-jcoqd-0ddcfc915a.json");

const FireBase = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://muaban-coin.firebaseio.com"
});

module.exports = {
  FireBase,
  getNewOrder,
  addBankHistory,
  setWithdrawal,
  updateOrderStatus,
  addUser,
  verifyToken,
  getUser,
  addPrice,
  getSystem,
  updatePrice,
  addOrder,
  getPublicChat,
  sendPublicChat,
  updateUser,
  findBankAccount,
  getUserHistory,
  changeNickname,
  getPriveteChatByID,
  sendPrivateChat,
  findAdminByUsername,
  updateSystem,
  searchUser,
  getAllMassage,
  deletePrivateChat,
  getHomeReportInfo,
  searchOrderByTarget,
  addNewAdmin,
  getListAdmin,
  updateAdminLastLogin,
  getOrder,
  getOrderByKey,
  saveBankingAPI,
  listenWithdrawal,
  removeWithdrawal
};

function removeWithdrawal(key) {
  return FireBase.firestore().doc(`WITHDRAWAL/${key}`).delete()
}

function listenWithdrawal() {
  return FireBase.firestore().collection("WITHDRAWAL").where("ID", ">", "0")
}

function saveBankingAPI(data) {
  data.time = new Date().getTime();
  return FireBase.firestore().doc(`BANKAPI/${data.time}`).set({ FROM: data.from, TO: data.to, VALUE: data.value, TIME: data.time, NOTE: data.note })
}

function getOrderByKey(key) {
  return FireBase.firestore().collection("ORDER").where("KEY", "==", key).get()
    .then(querySnapshot => {
      let order = false;
      querySnapshot.forEach(function (doc) {
        order = doc.data();
      });
      return order;
    })
}

function getOrder(key) {
  return FireBase.firestore().doc(`ORDER/${key}`).get().then(d => d.data())
}

function updateAdminLastLogin(username) {
  return FireBase.firestore().doc(`ADMINACCOUNT/${username}`).update({ LASTLOGIN: new Date().getTime() })
}

function getListAdmin() {
  return FireBase.firestore().collection(`ADMINACCOUNT`).get()
    .then(data => {
      let arr = []
      data.forEach(function (doc) {
        let temp = Object.assign({}, doc.data());
        temp.PASSWORD = undefined;
        arr.push(temp);
      });
      return arr.sort((a, b) => {
        if (a.LASTLOGIN > b.LASTLOGIN) return -1;
        if (a.LASTLOGIN < b.LASTLOGIN) return 1;
        return 0;
      });
    })
}

function addNewAdmin(obj) {
  return FireBase.firestore().doc(`ADMINACCOUNT/${obj.USERNAME}`).set(obj)
}

function searchOrderByTarget(target, info) {
  if (target == "CREATETIME" || target == "ENDTIME") {
    let time = new Date().getTime();
    time -= parseFloat(info) * 2592000000;
    return FireBase.firestore().collection(`ORDER`).where(target, ">=", time).get()
      .then(querySnapshot => {
        let arr = []
        querySnapshot.forEach(function (doc) {
          arr.push(doc.data())
        });
        return arr.sort((a, b) => {
          if (a.CREATETIME > b.CREATETIME) return -1;
          if (a.CREATETIME < b.CREATETIME) return 1;
          return 0;
        });
      })
  }
  if (target == "STATUS" || target == "TOTAL") {
    return FireBase.firestore().collection(`ORDER`).where(target, "==", parseInt(info)).get()
      .then(querySnapshot => {
        let arr = []
        querySnapshot.forEach(function (doc) {
          arr.push(doc.data())
        });
        return arr.sort((a, b) => {
          if (a.CREATETIME > b.CREATETIME) return -1;
          if (a.CREATETIME < b.CREATETIME) return 1;
          return 0;
        });
      })
  }
  return FireBase.firestore().collection(`ORDER`).where(target, "==", info).get()
    .then(querySnapshot => {
      let arr = []
      querySnapshot.forEach(function (doc) {
        arr.push(doc.data())
      });
      return arr.sort((a, b) => {
        if (a.CREATETIME > b.CREATETIME) return -1;
        if (a.CREATETIME < b.CREATETIME) return 1;
        return 0;
      });
    })
}

function getHomeReportInfo() {
  return FireBase.firestore().collection("ORDER").get()
    .then((data) => {
      let result = {
        maxBuyBTC: { value: -Infinity, key: "" },
        minBuyBTC: { value: Infinity, key: "" },
        maxBuyVND: { value: -Infinity, key: "" },
        minBuyVND: { value: Infinity, key: "" },
        maxSellBTC: { value: -Infinity, key: "" },
        minSellBTC: { value: Infinity, key: "" },
        maxSellVND: { value: -Infinity, key: "" },
        minSellVND: { value: Infinity, key: "" },
        totalOrder: data.docs.length,
        totalCompleteOrder: 0,
        totalCancelOrder: 0,
        totalInProcessOrder: 0,
        totalWaitingOrder: 0,
        totalInBTC: 0,
        totalOutBTC: 0,
        totalInVND: 0,
        totalOutVND: 0
      }
      data.forEach(function (change) {
        let data = change.data();
        data.TOTAL = parseInt(data.TOTAL);
        data.AMOUNT = parseFloat(data.AMOUNT);
        let isBUY = data.TYPE == "BUY";
        if (data.STATUS == -1) {
          result.totalCancelOrder++;
        } else if (data.STATUS == 1) {
          result.totalInProcessOrder++;
          if (isBUY) {
            result.totalInVND += data.TOTAL;
          } else {
            result.totalInBTC += data.AMOUNT;
          }
        } else if (data.STATUS == 2) {
          result.totalCompleteOrder++;
          if (isBUY) {
            result.totalInVND += data.TOTAL;
            result.totalOutBTC = +(result.totalOutBTC + data.AMOUNT).toFixed(12);
            if (data.AMOUNT > result.maxBuyBTC.value) {
              result.maxBuyBTC.value = data.AMOUNT;
              result.maxBuyBTC.key = data.KEY;
            }
            if (data.AMOUNT < result.minBuyBTC.value) {
              result.minBuyBTC.value = data.AMOUNT;
              result.minBuyBTC.key = data.KEY;
            }
            if (data.TOTAL > result.maxBuyVND.value) {
              result.maxBuyVND.value = data.TOTAL;
              result.maxBuyVND.key = data.KEY;
            }
            if (data.TOTAL < result.minBuyVND.value) {
              result.minBuyVND.value = data.TOTAL;
              result.minBuyVND.key = data.KEY;
            }
          } else {
            result.totalInBTC = +(result.totalInBTC + data.AMOUNT).toFixed(12);
            result.totalOutVND += data.TOTAL;
            if (data.AMOUNT > result.maxSellBTC.value) {
              result.maxSellBTC.value = data.AMOUNT;
              result.maxSellBTC.key = data.KEY;
            }
            if (data.AMOUNT < result.minSellBTC.value) {
              result.minSellBTC.value = data.AMOUNT;
              result.minSellBTC.key = data.KEY;
            }
            if (data.TOTAL > result.maxSellVND.value) {
              result.maxSellVND.value = data.TOTAL;
              result.maxSellVND.key = data.KEY;
            } else if (data.TOTAL < result.minSellVND.value) {
              result.minSellVND.value = data.TOTAL;
              result.minSellVND.key = data.KEY;
            }
          }
        } else if (data.STATUS == 0) {
          result.totalWaitingOrder++;
        }
      })
      return result;
    })
}

function deletePrivateChat(ID) {
  return FireBase.firestore().collection("PRIVATECHAT").where("ID", "==", ID).get()
    .then(data => {
      data.docChanges.forEach(function (change) {
        return FireBase.firestore().doc(`PRIVATECHAT/${change.doc.id}`).delete();
      })
    })
}

function getAllMassage() {
  return FireBase.firestore().collection("PRIVATECHAT").where("TIME", ">=", 0).get()
    .then(querySnapshot => {
      let arr = []
      let arrTemp = [];
      querySnapshot.forEach(function (doc) {
        let data = doc.data();
        let index = arrTemp.indexOf(data.ID);
        if (index == -1) {
          arrTemp.push(data.ID)
          arr.push({ ID: data.ID, NICKNAME: data.NICKNAME, LIST: [data] });
        } else {
          arr[index].LIST.push(data);
        }
      });
      return arr;
    })
}

function addUser(key, obj) {
  return FireBase.firestore().doc(`USER/${key}`).set(obj)
}

function verifyToken(token) {
  return FireBase.auth().verifyIdToken(token)
}

function getUser(key) {
  return FireBase.firestore().doc(`USER/${key}`).get()
    .then(d => d.data())
}

function addPrice(price, type) {
  let time = new Date().getTime();
  return FireBase.firestore().doc(`PRICE/${time}`).set({ VALUE: price, TIME: time, TYPE: type })
}

function getSystem(cb) {
  return FireBase.firestore().collection("SYSTEM").onSnapshot(data => cb(data));
}

function getNewOrder(cb) {
  return FireBase.firestore().collection("ORDER").where("STATUS", "<", 2).onSnapshot(data => cb(data));
}

function updatePrice(type, price) {
  return FireBase.firestore().doc(`SYSTEM/SYSTEM`).update({ [type]: parseFloat(price) })
}

function addOrder(obj) {
  let key = new Date().getTime();
  obj.KEY = key.toString(16);
  obj.CREATETIME = key;
  if (obj.type == "BUY") {
    obj.ACCOUNT = getAccount();
    return FireBase.firestore().doc(`ORDER/${key}`).set(obj).then(() => obj)
  } else {
    let info = newBTCWallet();
    obj.WALLET = info.wallet;
    obj.PRIVATEKEY = info.privateKey;
    return FireBase.firestore().doc(`ORDER/${key}`).set(obj).then(() => {
      obj.PRIVATEKEY = undefined;
      return obj;
    })
  }
}

function getPublicChat() {
  let time = new Date().getTime();
  return FireBase.firestore().collection("PUBLICCHAT").orderBy("TIME", "desc").limit(10).get()
    .then((data) => {
      let arr = [];
      data.docChanges.forEach(function (change) {
        arr.push(change.doc.data());
      });
      return arr.reverse();
    })
}

function sendPublicChat(obj) {
  return FireBase.firestore().collection("PUBLICCHAT").add(obj)
    .catch(err => {
      console.log(err.toString())
      addLogToList({ folder: "Firebase", text: `Error - sendPublicChat=${err.toString()}` });
    })
}

function updateUser(key, obj) {
  return FireBase.firestore().doc(`USER/${key}`).update(obj)
}

function updateOrderStatus(key, status, id) {
  let socket = getSocketByID(id);
  if (socket) {
    let temp = JSON.stringify({ CREATETIME: key, STATUS: status })
    socket.emit("UPDATE_HISTORY", temp);
  }
  if (status == 2) return FireBase.firestore().doc(`ORDER/${key}`).update({ STATUS: status, ENDTIME: new Date().getTime() })
  else return FireBase.firestore().doc(`ORDER/${key}`).update({ STATUS: status })
}

function setWithdrawal(obj) {
  return FireBase.firestore().doc(`WITHDRAWAL/${obj.CREATETIME}`).set(obj)
}

function findBankAccount(ID) {
  return FireBase.firestore().doc(`BANKACCOUNT/${ID}`).get().then(d => d.data())
}

function addBankHistory(from, to, value, note) {
  let time = new Date().getTime();
  return FireBase.firestore().collection(`BANKHISTORY`).add({ from, to, value, time, note })
}

function getUserHistory(id, amount) {
  return FireBase.firestore().collection("ORDER").where("ID", "==", id).orderBy("CREATETIME", "desc").limit(amount).get()
    .then(querySnapshot => {
      let arr = []
      querySnapshot.forEach(function (doc) {
        arr.push(doc.data())
      });
      return arr;
    })
}

function changeNickname(id, nickname) {
  return FireBase.firestore().collection("PUBLICCHAT").where("ID", "==", id).get()
    .then(data => {
      data.docChanges.forEach(function (change) {
        return FireBase.firestore().doc(`PUBLICCHAT/${change.doc.id}`).update({ NICKNAME: nickname })
      })
      return FireBase.firestore().collection("PRIVATECHAT").where("ID", "==", id).get()
        .then(result => {
          result.docChanges.forEach(function (change) {
            return FireBase.firestore().doc(`PRIVATECHAT/${change.doc.id}`).update({ NICKNAME: nickname })
          })
        })
    })
}

function getPriveteChatByID(id) {
  return FireBase.firestore().collection(`PRIVATECHAT`).where("ID", "==", id).get()
    .then(querySnapshot => {
      let arr = []
      querySnapshot.forEach(function (doc) {
        arr.push(doc.data())
      });
      return arr;
    })
}

function sendPrivateChat(obj) {
  return FireBase.firestore().doc(`PRIVATECHAT/${obj.ID}${obj.TIME}`).set(obj)
    .catch(err => {
      console.log(err.toString())
      addLogToList({ folder: "Firebase", text: `Error - sendPrivateChat=${err.toString()}` });
    })
}

function findAdminByUsername(username) {
  return FireBase.firestore().collection(`ADMINACCOUNT`).where("USERNAME", "==", username).get()
    .then(querySnapshot => {
      let result = false;
      querySnapshot.forEach(function (doc) {
        result = doc.data();
        return false;
      });
      return result;
    })
}

function updateSystem(data) {
  return FireBase.firestore().doc("SYSTEM/SYSTEM").update(data)
}

function searchUser(target, info) {
  return FireBase.firestore().collection(`USER`).where(target, "==", info).get()
    .then(querySnapshot => {
      let arr = []
      querySnapshot.forEach(function (doc) {
        arr.push(doc.data())
      });
      return arr;
    })
}