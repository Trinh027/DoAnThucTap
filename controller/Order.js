let { checkBalance } = require("./BTC.js");
let { addLogToList } = require("./LogWriter.js")

let orderList = [];

function listenOrder(cb) {
    let { getNewOrder } = require("./Firebase.js");
    return getNewOrder(data => {
        data.docChanges.forEach(function (change) {
            let temp = change.doc.data();
            if (temp.TYPE == "SELL" && !isInOrderList(temp.WALLET)) {
                return checkBalance(temp, 2000);
            } else {
                if (temp.STATUS == 0) {
                    return checkBuyOrder(temp);
                }
            }
        });
    })
}

function checkBuyOrder(obj) {
    let { updateOrderStatus, getOrder } = require("./Firebase.js");
    let checktime = new Date().getTime();
    if (obj.STATUS == 0 && checktime - obj.CREATETIME >= 7200000) {
        return updateOrderStatus(obj.CREATETIME, -1, obj.ID)
            .catch(err => {
                addLogToList({ folder: "Order", text: `Error - updateOrderStatus=${err.toString()}` });
                console.log(err)
            })
    }
    if (obj.STATUS == 0) {
        return setTimeout(() => {
            return getOrder(obj.CREATETIME)
                .then(data => {
                    return checkBuyOrder(data)
                })
                .catch(err => {
                    addLogToList({ folder: "Order", text: `Error - getOrder=${err.toString()}` });
                    console.log(err)
                })
        }, 7200000 - (checktime - obj.CREATETIME))
    }
}

function isInOrderList(key) {
    let temp = orderList.indexOf(key);
    if (temp == -1) {
        orderList.push(key);
        return false;
    }
    return true;
}

function removeFromOrderList(key) {
    let index = orderList.indexOf(key);
    if (index != -1) {
        orderList.splice(index, 1);
    }
}

function sendNewOrder(socket, order) {
    return socket.emit("NEW_ORDER", JSON.stringify(order));
}

module.exports = { listenOrder, sendNewOrder, checkBuyOrder, removeFromOrderList }