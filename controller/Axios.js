const axios = require("axios");
let bankAPI = "http://localhost:3000";
const config = {
    headers: {
        'Cache-Control': 'private',
        'Content-Type': 'application/json, text/html, charset=utf-8',
        'Content-Encoding': 'gzip',
        'Vary': 'Accept-Encoding',
        'X-AspNet-Version': '4.0.30319',
        'X-AspNetMvc-Version': '4.0',
        'X-Powered-By': 'ASP.NET',
        'Accept': 'application/json',
        'Server': 'Microsoft-IIS/7.5'
    }
};

function GET(url) {
    return axios.get(url)
}

function checkBankAccount(ID) {
    return axios.get(`${bankAPI}/checkAccount/${ID}`)
        .then(function (response) {
            return response.data;
        })
}


function POST(url, body, res) {
    let time = new Date().getTime();
    body.apiKey = `${time * 391.852456951357}`
    if (res) {
        return axios.post(url, body)
            .then(function (response) {
                return res.send(response.data)
            })
            .catch(function (error) {
                console.log(error.toString())
                return res.send({ KETQUA: 0, NOTE: "Error_US04" })
            });
    } else {
        return axios.post(url, body)
    }

}

function transferMoney(Account, amount, key) {
    let Root = require("../controller/System.js").getBankAccount();
    let obj = {
        from: Root,
        to: Account,
        value: amount,
        note: `Sell Order ${key}`,
    }
    return axios.post(`${bankAPI}/transfer`, obj)
        .then((response) => {
            return response.data;
        })
}

module.exports = { GET, POST, checkBankAccount, transferMoney };