let bitcore;
try {
    bitcore = require('bitcore-explorers/node_modules/bitcore-lib');
} catch (error) {
    bitcore = require('bitcore-lib');
}
module.exports = { bitcore }