let SYSTEM = undefined;

module.exports = {
    loadSystem,
    isSystem,
    updateValue,
    getPrice,
    getAccount,
    getTransferFee,
    getBankAccount,
    getSystemData,
    getWalletPrivatekeyFee
};

function loadSystem(cb) {
    let { getSystem } = require("./Firebase.js");
    return getSystem(data => {
        if (SYSTEM) {
            data.docChanges.forEach(function (change) {
                if (change.type === "added") {
                    SYSTEM = change.doc.data();
                }
                if (change.type === "modified") {
                    let tam = change.doc.data();
                    for (let key in tam) {
                        if (key != "BUYPRICE" && key != "SELLPRICE" && SYSTEM[key] != tam[key]) {
                            SYSTEM[key] = tam[key];
                        }
                    }
                }
                // if (change.type === "removed") {

                // }
            });
        } else {
            SYSTEM = {};
            data.docChanges.forEach(function (change) {
                if (change.type === "added") {
                    SYSTEM = Object.assign({}, change.doc.data());
                }
                // if (change.type === "modified") {

                // }
                // if (change.type === "removed") {

                // }
            });
            cb(SYSTEM.SELLPRICE, SYSTEM.BUYPRICE);
        }
    })
}

function updateValue(type, price, cb) {
    let { updatePrice } = require("./Firebase.js");
    let key = "";
    let value;
    if (type == 1 && !SYSTEM.LOCKSELLPRICE) {
        value = parseInt(price * SYSTEM.FEESELL * SYSTEM.VND);
        SYSTEM.SELLPRICE = value;
        key = "SELLPRICE";
        cb(key, value);
        return updatePrice(key, value);
    }
    if (!SYSTEM.LOCKBUYPRICE) {
        value = parseInt(price * SYSTEM.FEEBUY * SYSTEM.VND);
        SYSTEM.BUYPRICE = value;
        key = "BUYPRICE";
        cb(key, value);
        return updatePrice(key, value);
    }
}

function isSystem() {
    if (SYSTEM) {
        return true;
    } else {
        return false;
    }
}

function getPrice() {
    let { BUYPRICE, SELLPRICE } = SYSTEM;
    return { BUYPRICE, SELLPRICE, TRANSFERFEE: SYSTEM.TRANSFERFEE / 100000000 };
}

function getAccount() {
    return SYSTEM.ACCOUNT;
}

function getTransferFee() {
    return SYSTEM.TRANSFERFEE;
}

function getBankAccount() {
    return SYSTEM.ACCOUNT;
}

function getSystemData() {
    return SYSTEM;
}

function getWalletPrivatekeyFee() {
    let { WALLET, PRIVATEKEY, TRANSFERFEE } = SYSTEM;
    return { WALLET, PRIVATEKEY, TRANSFERFEE };
}