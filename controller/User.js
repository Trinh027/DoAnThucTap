let USER = {};

function onlineUser(ID, socket) {
    USER[ID] = socket;
};

function offlineUser(ID) {
    USER[ID] = undefined;
};

function checkSocket(ID, socketID) {
    if (USER[ID]) {
        if (USER[ID].id == socketID) return true;
    }
    return false;
}

function getSocketByID(ID) {
    return USER[ID];
}

function checkUser(ID) {
    if (USER[ID]) {
        return true;
    } else {
        return false;
    }
};

function deletePrivateMassage(ID) {
    for (let key in USER) {
        if (isNaN(key) && USER[key]) {
            USER[key].emit("DELETE_PRIVATECHAT", JSON.stringify({ ID: ID }));
        }
    }
}

function sendPrivateMassage(massage) {
    let user = USER[massage.ID]
    let massa = JSON.stringify(massage);
    if (user) {
        user.emit("SEND_PRIVATECHAT", massa);
    }
    // Gui cho tat ca Admin dang online
    for (let key in USER) {
        if (isNaN(key) && USER[key]) {
            USER[key].emit("SEND_PRIVATECHAT", massa);
        }
    }
}

module.exports = { onlineUser, offlineUser, checkUser, checkSocket, getSocketByID, sendPrivateMassage, deletePrivateMassage };