let fs = require("fs");

let listNeedToWrite = [];
let isRun = false;


module.exports = { addLogToList }

function addLogToList(log) {
    let data = new Date();
    let time = `${data.getHours()}:${data.getMinutes()}:${data.getSeconds()} `
    log.text = time + log.text;
    listNeedToWrite.push(log);
    return writeLog()
}

function writeLog() {
    if (listNeedToWrite.length == 0) return run = false;
    let temp = listNeedToWrite[0];
    run = true;
    let time = new Date();
    let name = `${temp.folder}-${time.getDate()}-${time.getMonth() + 1}-${time.getFullYear()}`;
    let text = `${temp.text}\n`;
    let patch = `./log/${name}.txt`;
    return fs.readFile(patch, (err, data) => {
        let writeText;
        if (err) {
            writeText = text;
        } else {
            writeText = data + text;
        }
        return fs.writeFile(patch, writeText, (err) => {
            listNeedToWrite.splice(0, 1);
            return writeLog();
        })
    })
}