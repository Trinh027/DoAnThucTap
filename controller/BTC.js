let { transferMoney } = require("./Axios.js");
let { Address, PrivateKey, Transaction, PublicKey } = require('./bitcore.js').bitcore;
let { Insight } = require('bitcore-explorers');
let { getUnspentOutputs, getAddress, getBalance } = require('blockchain.info/blockexplorer');
let { addLogToList } = require("./LogWriter.js")

let arrCheck = []

function BTCPrivateKey() {
    var privateKey = new PrivateKey();
    return privateKey;
}

function BTCPublickKey(privateKey) {
    var publicKey = new PrivateKey(privateKey).toPublicKey();
    return publicKey;
}

function BTCWallet(publicKey) {
    var wallet = new Address(publicKey)
    return wallet;
}

function newBTCWallet() {
    var privateKey = new PrivateKey().toString()
    var publicKey = new PrivateKey(privateKey).toPublicKey().toString();
    var xpub = new PublicKey(publicKey);
    var wallet = new Address(xpub).toString();
    return { wallet, privateKey, publicKey };
}

function transferBTC(from, to, Amount, privateKey, fee) {
    console.log(from, to, Amount, privateKey, fee)
    return new Promise((resolve, reject) => {
        let tam = `${from}${to}${Amount}${privateKey}${fee}`;
        if (arrCheck.indexOf(tam) != -1) {
            return reject("loop");
        }
        let index = arrCheck.push(tam);
        Amount = parseInt(Amount * 100000000);
        return getBalance(from).then(d => {
            console.log(d)
            return parseInt(d[from]['final_balance'], 10)
        })
            .then(balance => {
                let satoshi = Amount;
                if (satoshi + fee > balance) return reject("Not enough Bitcoin");
                let prikey = new PrivateKey(privateKey);
                let ins = new Insight("livenet");
                let tx = new Transaction();
                ins.getUnspentUtxos(from, (err, utxos) => {
                    if (err) {
                        addLogToList({ folder: "BTC", text: `Error - getUnspentUtxos=${err.toString()}` });
                        return reject(err)
                    };
                    try {
                        tx = tx.from(utxos).to(to, satoshi).fee(parseInt(fee));
                        tx = tx.change(from);
                        tx = tx.sign(prikey);
                        tx = tx.serialize();
                        ins.broadcast(tx, (err, txId) => {
                            if (err) {
                                addLogToList({ folder: "BTC", text: `Error - broadcast=${err.toString()}` });
                                return reject(err)
                            } else {
                                arrCheck.splice(index, 1);
                                return resolve(txId);
                            }
                        });
                    } catch (err) {
                        addLogToList({ folder: "BTC", text: `Error - UnexpectBroadcast=${err.toString()}` });
                        return reject(err)
                    };
                });
            })
            .catch(err => {
                addLogToList({ folder: "BTC", text: `Error - getBalance=${err.toString()}` });
                return reject(err)
            });
    })
}

function checkBalance(obj, time) {
    let { updateOrderStatus, setWithdrawal } = require("./Firebase.js");
    setTimeout(() => {
        let checktime = new Date().getTime();
        if (checktime - obj.CREATETIME < 7200000) {
            return getBalance(from).then(d => parseInt(d[from]['final_balance'], 10))
                .then(balance => {
                    if (balance && balance > 0) {
                        if (parseInt(balance) / parseInt(obj.AMOUNT * 100000000) >= 1) {
                            return updateOrderStatus(obj.CREATETIME, 1, obj.ID)
                                .then(() => {
                                    let { ID, KEY, CREATETIME, WALLET, STATUS, AMOUNT, TYPE, PRIVATEKEY } = obj;
                                    return setWithdrawal({ ID, KEY, CREATETIME, WALLET, AMOUNT, TYPE, PRIVATEKEY })
                                })
                                .then(() => {
                                    return updateOrderStatus(obj.CREATETIME, 2, obj.ID);
                                })
                                .then(() => {
                                    return transferMoney(obj.ACCOUNT, obj.TOTAL, obj.KEY)
                                })
                                .then((result) => {
                                    console.log("Done!", obj.WALLET);
                                    let { removeFromOrderList } = require("./Order.js");
                                    return removeFromOrderList(obj.WALLET);
                                })
                                .catch((err) => {
                                    console.log(err.toString())
                                    addLogToList({ folder: "BTC", text: `Error - InupdateOrderStatus=${err.toString()}` });
                                    return checkBalance(obj, time);
                                })
                        } else {
                            return updateOrderStatus(obj.CREATETIME, 3, obj.ID)
                                .catch(() => {
                                    addLogToList({ folder: "BTC", text: `Error - updateOrderStatus=${err.toString()}` });
                                    return checkBalance(obj, time)
                                })
                        }
                    } else {
                        return checkBalance(obj, time)
                    }
                })
                .catch(() => {
                    return checkBalance(obj, time)
                })
        } else {
            return updateOrderStatus(obj.CREATETIME, 3, obj.ID)
                .catch((err) => {
                    addLogToList({ folder: "BTC", text: `Error - updateOrderStatus=${err.toString()}` });
                    return checkBalance(obj, time)
                })
        }
    }, time)
}

module.exports = { newBTCWallet, transferBTC, checkBalance };

