var Action = {};

function checkAction(obj) {
    var { ID, action, time } = obj;
    if (!Action[ID]) {
        Action[ID] = {};
        Action[ID][action] = time;
        return true;
    } else {
        if (!Action[ID][action] || time - Action[ID][action] <= 10000) {
            Action[ID][action] = time;
            return true;
        } else {
            return false;
        }
    }
}

function uncheckAction(obj) {
    var { ID, action } = obj;
    Action[ID][action] = false;
}

module.exports = { checkAction, uncheckAction };