const System = require("./System.js");
let { transferBTC } = require("./BTC.js");
let { listenWithdrawal, removeWithdrawal, updateOrderStatus, getOrder } = require("./Firebase.js");
let { addLogToList } = require("./LogWriter.js")

let withdrawl = undefined;

function loadWithdrawal(cb) {
    return listenWithdrawal()
        .onSnapshot(snapshot => {
            if (withdrawl) {
                snapshot.docChanges.forEach(function (change) {
                    if (change.type === "added") {
                        withdrawl[change.doc.id] = change.doc.data();
                        withdrawCoin(Object.assign({}, change.doc.data()))
                    }
                    if (change.type === "modified") {
                        withdrawl[change.doc.id] = change.doc.data();
                    }
                    if (change.type === "removed") {
                        withdrawl[change.doc.id] = undefined;
                    }
                });
            } else {
                withdrawl = {};
                snapshot.docChanges.forEach(function (change) {
                    if (change.type === "added") {
                        withdrawl[change.doc.id] = change.doc.data();
                        withdrawCoin(Object.assign({}, change.doc.data()))
                    }
                    if (change.type === "modified") {
                        withdrawl[change.doc.id] = change.doc.data();
                    }
                    if (change.type === "removed") {
                        withdrawl[change.doc.id] = undefined;
                    }
                });
                cb()
            }
        })
}

function loopDelete(key) {
    return removeWithdrawal(key)
        .catch(err => {
            addLogToList({ folder: "Withdrawal", text: `Error - removeWithdrawal=${err.toString()}` });
            setTimeout(() => {
                return loopDelete(key)
            }, 10000)
        })
}

function withdrawCoin(obj) {
    let rootInfo = System.getWalletPrivatekeyFee();
    let from, to, value, fee, privatekey;
    if (obj.TYPE == "BUY") {
        from = rootInfo.WALLET;
        to = obj.WALLET;
        privatekey = rootInfo.PRIVATEKEY;
    } else {
        from = obj.WALLET;
        to = rootInfo.WALLET;
        privatekey = obj.PRIVATEKEY;
    }
    value = obj.AMOUNT;
    fee = rootInfo.TRANSFERFEE;
    return transferBTC(from, to, value, privatekey, fee)
        .then((data) => {
            return updateOrderStatus(obj.CREATETIME, 2, obj.ID);
        })
        .then(() => {
            return loopDelete(obj.CREATETIME)
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "Withdrawal", text: `Error - transferBTC=${err.toString()}` });
            if (err != "loop") {
                return withdrawCoin(obj)
            }
        })
}

function isWithdrawl() {
    if (ThuHoi) {
        return true;
    } else {
        return false;
    }
}

module.exports = { loadWithdrawal, isWithdrawl };