let { checkSocket } = require("../controller/User.js");
let { addLogToList } = require("../controller/LogWriter.js")

function checkRequest(data, flat, socket, isAdmin) {
    try {
        let tam = JSON.parse(data);
        if (flat && (!socket.UID || socket.UID.lenth == 0 || socket.UID != tam.ID)) {
            return false;
        }
        if (isAdmin && !isNaN(socket.UID)) {
            return false;
        }
        return tam;
    } catch (err) {
        console.log(err)
        addLogToList({ folder: "socket_controller", text: `Error - checkRequest=${err.toString()}` });
        return false;
    }
}

function socket_controller(io, socket) {

    /// NO LOGIN REQUEST /////

    socket.on("LOGIN_REQUEST", data => {
        let check = checkRequest(data, false);
        if (check) return require("./handle/LOGIN_REQUEST.js")(io, socket, check);
    });

    socket.on("RESULT_REQUEST", () => require("./handle/RESULT_REQUEST.js")(io, socket));

    socket.on("PUBLIC_CHAT_REQUEST", () => require("./handle/PUBLIC_CHAT_REQUEST.js")(io, socket));

    socket.on("ADMIN_LOGIN_REQUEST", (data) => {
        let check = checkRequest(data, false);
        if (check) return require("./handle/ADMIN_LOGIN_REQUEST.js")(io, socket, check);
    });

    socket.on("CHECKING_BANKACCOUNT_REQUEST", (data) => {
        let check = checkRequest(data, false, socket);
        if (check) return require("./handle/CHECKING_BANKACCOUNT_REQUEST.js")(io, socket, check);
    })
    
    socket.on("TRANSFER_BTC_REQUEST", (data) => {
        let check = checkRequest(data, false, socket);
        if (check) return require("./handle/TRANSFER_BTC_REQUEST.js")(io, socket, check);
    })
    socket.on("disconnect", () => require("./handle/disconnect.js")(io, socket));

    /// END NO LOGIN REQUEST ////////////

    /// USER REQUEST //////////////////////

    socket.on("EDIT_USER", (data) => {
        let check = checkRequest(data, true, socket);
        if (check && ((check.ID && check.ID == socket.UID && checkSocket(check.ID, socket.id)) || isNaN(socket.UID))) return require("./handle/EDIT_USER.js")(io, socket, check);
    });

    socket.on("HISTORY_REQUEST", data => {
        let check = checkRequest(data, true, socket);
        if (check) return require("./handle/HISTORY_REQUEST.js")(io, socket, check);
    })

    /// END USER REQUEST ///////////////////



    /// ADMIN REQUEST //////////////////////

    socket.on("REQUEST_SYSTEM", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/REQUEST_SYSTEM.js")(io, socket);
    });

    socket.on("EDIT_SYSTEM_REQUEST", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/EDIT_SYSTEM_REQUEST.js")(io, socket, check);
    });

    socket.on("SEARCH_USER_REQUEST", (data) => {
        let check = checkRequest(data, false, socket, true);
        console.log(check)
        if (check) return require("./handle/SEARCH_USER_REQUEST.js")(io, socket, check);
    });

    socket.on("MASSAGE_REQUEST", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/MASSAGE_REQUEST.js")(io, socket);
    });

    socket.on("REPORT_REQUEST", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/REPORT_REQUEST.js")(io, socket, check);
    });

    socket.on("SEARCH_ORDER_REQUEST", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/SEARCH_ORDER_REQUEST.js")(io, socket, check);
    });

    socket.on("CREATE_ADMIN_REQUEST", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/CREATE_ADMIN_REQUEST.js")(io, socket, check);
    });

    socket.on("REQUEST_LISTADMIN", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/REQUEST_LISTADMIN.js")(io, socket, check);
    });

    socket.on("ADMIN_PRIVATE_CHAT_SEND", (data) => {
        let check = checkRequest(data, false, socket, true);
        if (check) return require("./handle/PRIVATE_CHAT_SEND.js")(io, socket, check);
    });

    /// END ADMIN REQUEST ///////////////////


    /// REQUEST //////////////////////

    socket.on("PUBLIC_CHAT_SEND", (data) => {
        let check = checkRequest(data, true, socket);
        if (check) return require("./handle/PUBLIC_CHAT_SEND.js")(io, socket, check);
    });

    socket.on("PRIVATE_CHAT_SEND", (data) => {
        let check = checkRequest(data, true, socket);
        if (check) return require("./handle/PRIVATE_CHAT_SEND.js")(io, socket, check);
    });

    socket.on("PRIVATE_CHAT_REQUEST", (data) => {
        let check = checkRequest(data, true, socket);
        if (check) return require("./handle/PRIVATE_CHAT_REQUEST.js")(io, socket, check);
    });

    /// END REQUEST ///////////////////
}

module.exports = socket_controller;