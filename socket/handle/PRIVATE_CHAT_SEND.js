let { sendPrivateChat, deletePrivateChat } = require("../../controller/Firebase.js");
let { sendPrivateMassage, deletePrivateMassage } = require("../../controller/User.js")
let { addLogToList } = require("../../controller/LogWriter.js")

function PRIVATE_CHAT_SEND(io, socket, data) {
    data.TIME = new Date().getTime();
    data.SENDPERSON = socket.UID;
    data.CONTENT = data.CONTENT.replace(/<script|script>/gi, "");
    if (data.CONTENT == "/deletethis" && isNaN(socket.UID)) {
        return deletePrivateChat(data.ID)
            .then(() => {
                return deletePrivateMassage(data.ID)
            })
            .catch(err=>{
                addLogToList({ folder: "PRIVATE_CHAT_SEND", text: `Error - deletePrivateChat=${err.toString()} - ID=${socket.UID}` });
            })
    }
    return sendPrivateChat(data)
        .then(() => {
            return sendPrivateMassage(data);
        })
        .catch(err => {
            console.log(err.toString())
            addLogToList({ folder: "PRIVATE_CHAT_SEND", text: `Error - sendPrivateChat=${err.toString()} - ID=${socket.UID}` });
        })
}

module.exports = PRIVATE_CHAT_SEND;