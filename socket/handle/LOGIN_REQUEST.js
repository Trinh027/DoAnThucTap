let { verifyToken, getUser, addUser, updateUser } = require("../../controller/Firebase.js");
let User = require("../../controller/User.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function LOGIN_REQUEST(io, socket, data) {
    let { email, phoneNumber, uid, providerId, token, NGT } = data;
    if (!token || !uid || isNaN(uid)) {
        addLogToList({ folder: "LOGIN_REQUEST", text: `Error - uid=${uid}` });
        return socket.emit("LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Wrong Input!" }));
    }
    return verifyToken(token)
        .then(() => {
            if (User.checkUser(uid) || socket.UID) {
                addLogToList({ folder: "LOGIN_REQUEST", text: `Error - checkUser=${uid} - ID=${socket.UID}` });
                return socket.emit("LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Someone is using this account!" }));
            }
            return getUser(uid)
                .then(user => {
                    if (user) {
                        user.providerId = undefined;
                        socket.UID = uid;
                        User.onlineUser(uid, socket);
                        user.HISTORY = [];
                        LASTLOGIN = new Date().getTime();
                        updateUser(uid, { LASTLOGIN });
                        return socket.emit("LOGIN_RESPONSE", JSON.stringify({ STATUS: 1, token, USER: user }));
                    } else {
                        let createTime = new Date().getTime();
                        let userInfo = { ID: uid, EMAIL: email, PHONE: phoneNumber, providerId, CREATETIME: createTime, NICKNAME: uid, LASTLOGIN: createTime, STATUS: 0 };
                        return addUser(uid, userInfo)
                            .then(() => {
                                userInfo.providerId = undefined;
                                socket.UID = uid;
                                User.onlineUser(uid, socket);
                                userInfo.HISTORY = [];
                                return socket.emit("LOGIN_RESPONSE", JSON.stringify({ STATUS: 1, token, USER: userInfo }));
                            })
                            .catch(err => {
                                console.error(err)
                                addLogToList({ folder: "LOGIN_REQUEST", text: `Error - addUser=${err.toString()} - ID=${socket.UID}` });
                                return socket.emit("LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Write Database Error !" }));
                            })
                    }
                })
                .catch(err => {
                    console.error(err)
                    addLogToList({ folder: "LOGIN_REQUEST", text: `Error - getUser=${err.toString()} - ID=${socket.UID}` });
                    return socket.emit("LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Database Error!" }));
                })
        })
        .catch(err => {
            console.error(err)
            addLogToList({ folder: "LOGIN_REQUEST", text: `Error - verifyToken=${err.toString()} - ID=${socket.UID}` });
            return socket.emit("LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Wrong Token!" }));
        })
}

module.exports = LOGIN_REQUEST;