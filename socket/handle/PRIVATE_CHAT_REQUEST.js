let { getPriveteChatByID } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function PRIVATE_CHAT_REQUEST(io, socket, data) {
    return getPriveteChatByID(data.ID)
    .then(result=>{
        return socket.emit("PRIVATE_CHAT_RESPONSE", JSON.stringify(result));
    })
    .catch(err=>{
        addLogToList({ folder: "PRIVATE_CHAT_REQUEST", text: `Error - getPriveteChatByID=${err.toString()} - ID=${socket.UID}` });
        return console.log(err.toString());
    })
}

module.exports = PRIVATE_CHAT_REQUEST;