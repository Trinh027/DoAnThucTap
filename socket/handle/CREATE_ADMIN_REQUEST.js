let { findAdminByUsername, addNewAdmin, updateAdminLastLogin } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function CREATE_ADMIN_REQUEST(io, socket, data) {
    let { username, password, ID } = data;
    if (username.length < 6 || username.length > 12 || username.length != username.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length || !isNaN(username)) {
        addLogToList({ folder: "CREATE_ADMIN_RESPONSE", text: `Error - username=${username} - ID=${socket.UID}` });
        return socket.emit("CREATE_ADMIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Wrong Username!" }))
    }
    if (password.length < 6 || password.length > 8 || password.length != password.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) {
        addLogToList({ folder: "CREATE_ADMIN_RESPONSE", text: `Error - password=${password} - ID=${socket.UID}` });
        return socket.emit("CREATE_ADMIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Wrong Password!" }))
    }
    return findAdminByUsername(username)
        .then((result) => {
            if (result) {
                addLogToList({ folder: "CREATE_ADMIN_RESPONSE", text: `Error - UsernameExisted=${username} - ID=${socket.UID}` });
                return socket.emit("CREATE_ADMIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Username is existed!" }))
            }
            let admin = {
                CREATER: ID,
                USERNAME: username,
                PASSWORD: password,
                CREATETIME: new Date().getTime()
            }
            return addNewAdmin(admin)
                .then(() => {
                    admin.PASSWORD = undefined;
                    return socket.emit("CREATE_ADMIN_RESPONSE", JSON.stringify({ STATUS: 1, NOTE: "Create New Admin Success!", ADMIN: admin }))
                })
                .catch(err => {
                    console.log(err)
                    addLogToList({ folder: "CREATE_ADMIN_RESPONSE", text: `Error - addNewAdmin=${err.toString()} - ID=${socket.UID}` });
                    return socket.emit("CREATE_ADMIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error When Add New Admin On DataBase!" }))
                })
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "CREATE_ADMIN_RESPONSE", text: `Error - findAdminByUsername=${err.toString()} - ID=${socket.UID}` });
            return socket.emit("CREATE_ADMIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error When Check Username On DataBase!" }))
        })
}

module.exports = CREATE_ADMIN_REQUEST;