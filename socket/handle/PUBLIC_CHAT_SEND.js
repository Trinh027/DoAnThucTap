let { sendPublicChat } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function PUBLIC_CHAT_SEND(io, socket, data) {
    let time = new Date().getTime();
    if (data.CONTENT && data.CONTENT.length == 0) {
        addLogToList({ folder: "PUBLIC_CHAT_SEND", text: `Error - CONTENT=${CONTENT} - ID=${socket.UID}` });
        return 0;
    }
    if (data.NICKNAME && data.NICKNAME.length < 4 && data.NICKNAME.length > 12) {
        addLogToList({ folder: "PUBLIC_CHAT_SEND", text: `Error - NICKNAME=${NICKNAME} - ID=${socket.UID}` });
        return 0;
    }
    if (isNaN(data.ID)) {
        data.STATUS = 1;
    }
    data.CONTENT = data.CONTENT.replace(/<script|script>/gi, "")
    data.TIME = time;
    sendPublicChat(data);
    return io.emit("SEND_PUBLICCHAT", JSON.stringify(data));
}

module.exports = PUBLIC_CHAT_SEND;