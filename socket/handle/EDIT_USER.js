let { updateUser, changeNickname } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")
let REGEX = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
let emailcheck = E => (typeof E === 'string' && E.length > 5 && E.length < 30 && REGEX.test(E)) ? E.toLowerCase() : false;

function EDIT_USER(io, socket, data) {
    let tam = {};
    if (data.email) {
        if (emailcheck(data.email)) {
            tam.EMAIL = data.email;
        } else {
            addLogToList({ folder: "EDIT_USER", text: `Error - email=${email} - ID=${sokcket.UID}` });
        }
    }
    if (data.phone) {
        if (!isNaN(data.phone)) {
            tam.PHONE = data.phone;
        } else {
            addLogToList({ folder: "EDIT_USER", text: `Error - phone=${phone} - ID=${sokcket.UID}` });
        }
    }
    if (data.nickname) {
        if (data.nickname.length >= 6 && data.nickname <= 12 && data.nickname.length == data.nickname.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) {
            tam.NICKNAME = data.nickname;
        } else {
            addLogToList({ folder: "EDIT_USER", text: `Error - nickname=${nickname} - ID=${sokcket.UID}` });
        }
    }
    return updateUser(data.ID, tam)
        .then(() => {
            if (tam.NICKNAME) {
                changeNickname(data.ID, tam.NICKNAME)
            }
            return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 1, USER: tam }));
        })
        .catch((err) => {
            console.log(err.toString)
            addLogToList({ folder: "EDIT_USER", text: `Error - updateUser=${err.toString()} - ID=${socket.UID}` });
            return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "DataBase Error" }));
        })
}

module.exports = EDIT_USER;