let { updateSystem } = require("../../controller/Firebase.js");
let { checkBankAccount } = require("../../controller/Axios.js");
let { addLogToList } = require("../../controller/LogWriter.js")
const isPrivateKey = pk => require('../../controller/bitcore.js').bitcore.PrivateKey.isValid(pk, 'livenet');
const isWallet = wallet => require('../../controller/bitcore.js').bitcore.Address.isValid(wallet, 'livenet');

function EDIT_SYSTEM_REQUEST(io, socket, data) {
    let { TRANSFERFEE, SELLPRICE, ACCOUNT, BUYPRICE, SELLFEE, BUYFEE, VND, LOCKSELLPRICE, LOCKBUYPRICE, WALLET, PRIVATEKEY } = data;
    if (LOCKSELLPRICE != undefined && typeof (LOCKSELLPRICE) != "boolean") {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - LOCKSELLPRICE=${LOCKSELLPRICE} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of LOCKSELLPRICE" }))
    }
    if (LOCKBUYPRICE != undefined && typeof (LOCKBUYPRICE) != "boolean") {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - LOCKBUYPRICE=${LOCKBUYPRICE} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of LOCKBUYPRICE" }))
    }
    if (TRANSFERFEE != undefined && isNaN(TRANSFERFEE)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - TRANSFERFEE=${TRANSFERFEE} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of TRANSFERFEE" }))
    }
    if (SELLPRICE != undefined && isNaN(SELLPRICE)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - SELLPRICE=${SELLPRICE} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of SELLPRICE" }))
    }
    if (BUYPRICE != undefined && isNaN(BUYPRICE)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - BUYPRICE=${BUYPRICE} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of BUYPRICE" }))
    }
    if (VND != undefined && isNaN(VND)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - VND=${VND} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of VND" }))
    }
    if (SELLFEE != undefined && isNaN(SELLFEE)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - SELLFEE=${SELLFEE} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of SELLFEE" }))
    }
    if (BUYFEE != undefined && isNaN(BUYFEE)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - BUYFEE=${BUYFEE} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of BUYFEE" }))
    }
    if (WALLET != undefined && isWallet(WALLET)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - WALLET=${WALLET} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of WALLET" }))
    }
    if (PRIVATEKEY != undefined && isPrivateKey(PRIVATEKEY)) {
        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - PRIVATEKEY=${PRIVATEKEY} - ID=${socket.UID}` });
        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of PRIVATEKEY" }))
    }
    if (ACCOUNT != undefined) {
        return checkBankAccount(ACCOUNT)
            .then(result => {
                return updateSystem(data)
                    .then(() => {
                        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 1 }))
                    })
                    .catch(err => {
                        console.log(err)
                        addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - updateSystem=${err.toString()} - ID=${socket.UID}` });
                        return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error When Update To DataBase" }));
                    })
            })
            .catch(err => {
                console.log(err.toString())
                addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - checkBankAccount=${err.toString()} - ID=${socket.UID}` });
                return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error Of ACCOUNT" }))
            })
    }
    return updateSystem(data)
        .then(() => {
            return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 1 }))
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "EDIT_SYSTEM_REQUEST", text: `Error - updateSystem=${err.toString()} - ID=${socket.UID}` });
            return socket.emit("EDIT_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error When Update To DataBase" }));
        })

}

module.exports = EDIT_SYSTEM_REQUEST;