let { getSystemData } = require("../../controller/System.js");

function REQUEST_SYSTEM(io, socket) {
    return socket.emit("SYSTEM_RESPONSE", JSON.stringify(getSystemData()))
}

module.exports = REQUEST_SYSTEM;