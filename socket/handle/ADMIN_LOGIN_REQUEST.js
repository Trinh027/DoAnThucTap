let { findAdminByUsername, updateAdminLastLogin } = require("../../controller/Firebase.js");
let User = require("../../controller/User.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function ADMIN_LOGIN_REQUEST(io, socket, data) {
    let { username, password } = data;
    if (username.length < 6 || username.length > 12 || !isNaN(username) || username.length != username.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) {
        addLogToList({ folder: "ADMIN_LOGIN_REQUEST", text: `Error - username=${username}` });
        return socket.emit("ADMIN_LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Username is not allowed" }));
    }
    if (password.length < 6 || password.length > 8 || password.length != password.replace(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, '').length) {
        addLogToList({ folder: "ADMIN_LOGIN_REQUEST", text: `Error - password=${password}` });
        return socket.emit("ADMIN_LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Password is not allowed" }));
    }
    return findAdminByUsername(username)
        .then(result => {
            if (!result || result.PASSWORD != password) {
                addLogToList({ folder: "ADMIN_LOGIN_REQUEST", text: `Error - Username=${username} - WrongPassword=${password}` });
                return socket.emit("ADMIN_LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Wrong Username or Password" }));
            }
            socket.UID = username;
            User.onlineUser(username, socket);
            updateAdminLastLogin(username)
            return socket.emit("ADMIN_LOGIN_RESPONSE", JSON.stringify({ STATUS: 1, USER: { USERNAME: result.USERNAME } }));
        })
        .catch(err => {
            console.log(err);
            addLogToList({ folder: "ADMIN_LOGIN_REQUEST", text: `Error - findAdminByUsername=${err.toString()}` });
            return socket.emit("ADMIN_LOGIN_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Error whe checking Username" }));
        })
}

module.exports = ADMIN_LOGIN_REQUEST;