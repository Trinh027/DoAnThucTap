let { searchOrderByTarget } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function SEARCH_ORDER_REQUEST(io, socket, data) {
    return searchOrderByTarget(data.target, data.info)
        .then(result => {
            return socket.emit("SEARCH_ORDER_RESPONE", JSON.stringify(result));
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "SEARCH_ORDER_REQUEST", text: `Error - searchOrderByTarget=${err.toString()} - ID=${socket.UID}` });
        })
}

module.exports = SEARCH_ORDER_REQUEST;