let { getPublicChat } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function PUBLIC_CHAT_REQUEST(io, socket) {
    return getPublicChat()
        .then(arr => {
            return socket.emit("PUBLIC_CHAT_RESPONSE", JSON.stringify(arr));
        })
        .catch(err => {
            addLogToList({ folder: "PUBLIC_CHAT_REQUEST", text: `Error - getPublicChat=${err.toString()}` });
            console.log(err.toString())
        })
}

module.exports = PUBLIC_CHAT_REQUEST;