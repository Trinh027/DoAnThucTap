let { getListAdmin } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function REQUEST_LISTADMIN(io, socket) {
    return getListAdmin()
        .then((result) => {
            return socket.emit("RESPONSE_LISTADMIN", JSON.stringify(result));
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "REQUEST_LISTADMIN", text: `Error - getListAdmin=${err.toString()} - ID=${socket.UID}` });
        })
}

module.exports = REQUEST_LISTADMIN;