let { getPrice, isSystem } = require("../../controller/System.js");

function RESULT_REQUEST(io, socket) {
    if (isSystem()) {
        socket.emit("RESULT_RESPONSE", JSON.stringify(getPrice()));
    };
}

module.exports = RESULT_REQUEST;