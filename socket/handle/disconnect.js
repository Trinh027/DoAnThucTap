let User = require("../../controller/User.js");

function disconnect(io, socket) {
    if (socket.UID) return User.offlineUser(socket.UID);
}

module.exports = disconnect;