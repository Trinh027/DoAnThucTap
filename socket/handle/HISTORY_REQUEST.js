let { getUserHistory } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function HISTORY_REQUEST(io, socket, data) {
    return getUserHistory(socket.UID, data.amount)
        .then(arr => {
            return socket.emit("HISTORY_RESPONSE", JSON.stringify({ HISTORY: arr }))
        })
        .catch(err => {
            console.log(err.toString())
            addLogToList({ folder: "HISTORY_REQUEST", text: `Error - getUserHistory=${err.toString()} - ID=${socket.UID}` });
        })
}

module.exports = HISTORY_REQUEST;