let { getAllMassage } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function MASSAGE_REQUEST(io, socket) {
    return getAllMassage()
        .then(result => {
            return socket.emit("MASSAGE_RESPONSE", JSON.stringify(result));
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "MASSAGE_REQUEST", text: `Error - getAllMassage=${err.toString()} - ID=${socket.UID}` });
        })
}

module.exports = MASSAGE_REQUEST;