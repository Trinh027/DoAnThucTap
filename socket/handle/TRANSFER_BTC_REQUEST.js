let { transferBTC } = require("../../controller/BTC.js")
let { getWalletPrivatekeyFee } = require("../../controller/System.js")
let { addLogToList } = require("../../controller/LogWriter.js")
const isPrivateKey = pk => require("../../controller/bitcore.js").bitcore.PrivateKey.isValid(pk, 'livenet');
const isWallet = wallet => require("../../controller/bitcore.js").bitcore.Address.isValid(wallet, 'livenet');

function TRANSFER_BTC_REQUEST(io, socket, data) {
    console.log(data)
    let { from, to, value, privatekey } = data;
    if (!from || !isWallet(from)) return socket.emit("TRANSFER_BTC_RESPONSE", JSON.stringify({ NOTE: "Your Wallet Is Wrong" }))
    if (!to || !isWallet(to)) return socket.emit("TRANSFER_BTC_RESPONSE", JSON.stringify({ STATUS: 0, NOTE: "Your Receive Wallet Is Wrong" }))
    if (!privatekey || !isPrivateKey(privatekey)) return socket.emit("TRANSFER_BTC_RESPONSE", JSON.stringify({ NOTE: "Your Privatekey Is Wrong" }))
    if (to == from) return socket.emit("TRANSFER_BTC_RESPONSE", JSON.stringify({ NOTE: "Both Wallet must be diffirent" }))
    if (isNaN(value)) return socket.emit("TRANSFER_BTC_RESPONSE", JSON.stringify({ NOTE: "Amount of Bitcoin must be number" }))
    return transferBTC(from, to, value, privatekey, getWalletPrivatekeyFee().TRANSFERFEE)
        .then(() => {
            return socket.emit("TRANSFER_BTC_RESPONSE", JSON.stringify({ NOTE: "Transfer Success" }))
        })
        .catch((err) => {
            return socket.emit("TRANSFER_BTC_RESPONSE", JSON.stringify({ NOTE: err.toString() }))
        })
}

module.exports = TRANSFER_BTC_REQUEST;