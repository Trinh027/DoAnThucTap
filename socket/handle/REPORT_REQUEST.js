let { getHomeReportInfo } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function REPORT_REQUEST(io, socket, data) {
    return getHomeReportInfo()
        .then(result => {
            return socket.emit("REPORT_RESPONSE", JSON.stringify(result));
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "REPORT_REQUEST", text: `Error - getHomeReportInfo=${err.toString()} - ID=${socket.UID}` });
        })
}

module.exports = REPORT_REQUEST;