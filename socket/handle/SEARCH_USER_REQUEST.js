let { searchUser } = require("../../controller/Firebase.js");
let { addLogToList } = require("../../controller/LogWriter.js")

function SEARCH_USER_REQUEST(io, socket, data) {
    return searchUser(data.target, data.info)
        .then(result => {
            return socket.emit("SEARCH_USER_RESPONSE", JSON.stringify(result));
        })
        .catch(err => {
            console.log(err)
            addLogToList({ folder: "SEARCH_USER_REQUEST", text: `Error - searchUser=${err.toString()} - ID=${socket.UID}` });
        })
}

module.exports = SEARCH_USER_REQUEST;