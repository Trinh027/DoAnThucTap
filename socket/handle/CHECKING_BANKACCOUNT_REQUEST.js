let { checkBankAccount } = require("../../controller/Axios.js")
let { addLogToList } = require("../../controller/LogWriter.js")

function CHECKING_BANKACCOUNT_REQUEST(io, socket, data) {
    if (!data || !data.ID || data.ID.length < 9) {
        addLogToList({ folder: "CHECKING_BANKACCOUNT_REQUEST", text: `Error - ID=${data.ID}` });
        return socket.emit("CHECKING_BANKACCOUNT_RESPONSE", JSON.stringify({ STATUS: 0 }));
    }
    return checkBankAccount(data.ID)
        .then(result => {
            return socket.emit("CHECKING_BANKACCOUNT_RESPONSE", JSON.stringify(result));
        })
        .catch(err => {
            console.log(err.toString())
            addLogToList({ folder: "CHECKING_BANKACCOUNT_REQUEST", text: `Error - checkBankAccount=${err.toString()}` });
            return socket.emit("CHECKING_BANKACCOUNT_RESPONSE", JSON.stringify({ STATUS: 0 }));
        })
}

module.exports = CHECKING_BANKACCOUNT_REQUEST;